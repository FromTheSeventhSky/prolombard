package com.fromtheseventhsky.lombards.adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.davemorrissey.labs.subscaleview.ImageSource;
import com.davemorrissey.labs.subscaleview.SubsamplingScaleImageView;
import com.fromtheseventhsky.lombards.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class ImagePreviewAdapter extends PagerAdapter {
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<String> mImages;
    private int mPrevPosition;
    private ArrayList<SubsamplingScaleImageView> mContainer;

    public ImagePreviewAdapter(Context context) {
        mContext = context;
        mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mContainer = new ArrayList<>();
    }

    public void setPrevPosition(int prevPosition) {
        mPrevPosition = prevPosition;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void resetScale() {
        mContainer.get(mPrevPosition).resetScaleAndCenter();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_preview, container, false);
        SubsamplingScaleImageView imageView = (SubsamplingScaleImageView) itemView.findViewById(R.id.imageView);
        imageView.setImage(ImageSource.resource(R.drawable.close_image));
        imageView.setMaxScale(3);
        mContainer.add(imageView);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public void setImage(int position){
        mContainer.get(position).setImage(ImageSource.resource(R.drawable.mark));
    }

    /*
    *       Завести arraylist равный числу изоображений, заполнить его нуллами,
    *       при инстантиации объекат нулл заменять на объект, при удалении на нулл, итого изображений дофига,
    *       предыдущую позицию легко можно очистить, хранить в памяти кучу изображений не надо
     */

}//32 кабинет

package com.fromtheseventhsky.lombards.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class TestAdapter extends RecyclerView.Adapter<TestAdapter.PersonViewHolder> {


    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_lombard, parent, false);
        PersonViewHolder vh = new PersonViewHolder(v);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        return vh;
    }

    @Override
    public void onBindViewHolder(PersonViewHolder holder, int position) {
        holder.address.setText("ddd");
    }

    @Override
    public int getItemViewType(int position) {
        if(position==19)
            return 1;
        return 0;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public int getItemCount() {
        return 20;
    }

    public class PersonViewHolder extends RecyclerView.ViewHolder {
        ImageView logo, banner, remove;
        TextView title, address, license, workDate;
        View newLogo, location, item;

        PersonViewHolder(View itemView) {
            super(itemView);
            itemView.findViewById(R.id.notification_text).setVisibility(View.GONE);
            logo = (ImageView) itemView.findViewById(R.id.image_ico);
            title = (TextView) itemView.findViewById(R.id.title);
            address = (TextView) itemView.findViewById(R.id.address);
            license = (TextView) itemView.findViewById(R.id.license_number);
            workDate = (TextView) itemView.findViewById(R.id.work_day);
            newLogo = itemView.findViewById(R.id.image_new);
            location = itemView.findViewById(R.id.map);
            item = itemView.findViewById(R.id.item);
            banner = (ImageView) itemView.findViewById(R.id.banner);
            remove = (ImageView) itemView.findViewById(R.id.image_remove);
            remove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    notifyItemRemoved(1);
                }
            });
            item.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
        }
    }
}

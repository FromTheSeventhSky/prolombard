package com.fromtheseventhsky.lombards.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.models.LoanInner;
import com.fromtheseventhsky.lombards.models.LoanRates;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LoanRatesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int TITLE = 0, CONTENT = 1;

    private ArrayList<Object> mLoans;

    private int innerColorPos;

    public LoanRatesAdapter(ArrayList<LoanRates> objects) {
        mLoans = new ArrayList<>();
        for (int i = 0; i < objects.size(); i++) {
            LoanRates tmp = objects.get(i);
            mLoans.add(tmp);
            ArrayList<LoanInner> inner = tmp.getLoans();
            for (int j = 0; j < inner.size(); j++) {
                mLoans.add(inner.get(j));
            }
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case TITLE:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ite_title_rates, parent, false);
                return new ListItemTitleHolder(v);
            case CONTENT:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rates, parent, false);
                return new ListItemHolder(v);
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case TITLE:
                ListItemTitleHolder bannerHolder = (ListItemTitleHolder) holder;
                LoanRates lr = (LoanRates) mLoans.get(position);
                if(position==0){
                    int padding = bannerHolder.title.getPaddingLeft();
                    bannerHolder.title.setPadding(padding, padding, padding, 0);
                }
                bannerHolder.title.setText(lr.getName());
                innerColorPos = 0;
                break;
            case CONTENT:
                ListItemHolder listItemHolder = (ListItemHolder) holder;
                LoanInner li = (LoanInner) mLoans.get(position);
                listItemHolder.title.setText(li.getName());
                listItemHolder.value.setText(li.getRate());
                if (li.getColor()==-1) {
                    if (innerColorPos % 2 == 1) {
                        li.setColor(listItemHolder.root.getResources().getColor(R.color.list_item_top_color));
                    } else {
                        li.setColor(listItemHolder.root.getResources().getColor(R.color.transparent));
                    }
                }
                listItemHolder.root.setBackgroundColor(li.getColor());
                innerColorPos++;
                break;

        }
    }

    @Override
    public int getItemCount() {
        return mLoans.size();
    }
    @Override
    public int getItemViewType(int position) {
        if (mLoans.get(position) instanceof LoanRates) {
            return TITLE;
        }
        return CONTENT;
    }

    public class ListItemHolder extends RecyclerView.ViewHolder {
        LinearLayout root;
        TextView title, value;

        ListItemHolder(View itemView) {
            super(itemView);
            root = (LinearLayout) itemView.findViewById(R.id.root);
            title = (TextView) itemView.findViewById(R.id.title);
            value = (TextView) itemView.findViewById(R.id.value);
        }
    }

    public class ListItemTitleHolder extends RecyclerView.ViewHolder {
        TextView title;
        ListItemTitleHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView;
        }
    }
}

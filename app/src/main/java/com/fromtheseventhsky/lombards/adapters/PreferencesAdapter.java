package com.fromtheseventhsky.lombards.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.models.PreferenceItem;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class PreferencesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {

    private ArrayList<PreferenceItem> mPreferenceItems;

    public PreferencesAdapter(ArrayList<PreferenceItem> preferenceItems){
        mPreferenceItems = preferenceItems;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return 0;
    }
}

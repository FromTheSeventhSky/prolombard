package com.fromtheseventhsky.lombards.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.LombardApp;
import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.constants.ApiConst;
import com.fromtheseventhsky.lombards.models.AdItem;
import com.fromtheseventhsky.lombards.models.LombardItem;
import com.fromtheseventhsky.lombards.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LombardsAdapter extends BaseAdapter {

    private List<Object> mObjects;
    private ClickInterface mClickInterface;
    private LombardFilter mFilter;


    public LombardsAdapter(ArrayList<Object> objects, ClickInterface clickInterface) {
        mClickInterface = clickInterface;
        mObjects = objects;
    }

    public void addAll(ArrayList<Object> objects) {
        if (objects == null) {
            return;
        }
        mObjects.clear();
        mObjects.addAll(objects);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case BANNER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_banner, parent, false);
                BannerHolder bannerHolder = new BannerHolder(v);
                return bannerHolder;
            case LIST_ITEM:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_lombards, parent, false);
                ListItemHolder listItemHolder = new ListItemHolder(v);
                return listItemHolder;
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case BANNER: {
                BannerHolder bannerHolder = (BannerHolder) holder;
                AdItem item = (AdItem) mObjects.get(position);
                bannerHolder.bannerClickListener.setPosition(item);
                Picasso.with(LombardApp.sAppContext).load(item.getSmallUrl()).into(bannerHolder.banner);
            }
                break;
            case LIST_ITEM:
                ListItemHolder listItemHolder = (ListItemHolder) holder;
                LombardItem item = (LombardItem) mObjects.get(position);
                listItemHolder.title.setText(item.getLombardName());
                listItemHolder.address.setText(item.getLombardAddress());
                listItemHolder.license.setText(item.getLicenseNumber());
                listItemHolder.workDate.setText(Utils.parseWorkTime(item.getWorkTime()));
                listItemHolder.itemClickListener.setLombardItem(item);
                listItemHolder.mapClickListener.setLombardItem(item);
                if(!item.getIco().equalsIgnoreCase("null")){
                    Picasso.with(LombardApp.sAppContext).load(ApiConst.MAIN_URL+item.getIco()).into(listItemHolder.logo);
                }
                break;

        }
    }

    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    @Override
    public int getItemViewType(int position) {
        if (mObjects.get(position) instanceof LombardItem) {
            return LIST_ITEM;
        }
        return BANNER;
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new LombardFilter(mObjects);
        }
        return mFilter;
    }

    @Override
    void clearObj() {
        mObjects.clear();
    }

    @Override
    void addObj(Object obj) {
        mObjects.add(obj);
    }


    public class ListItemHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView title, address, license, workDate;
        View newLogo, location, item;
        MapClickListener mapClickListener;
        ItemClickListener itemClickListener;

        ListItemHolder(View itemView) {
            super(itemView);
            logo = (ImageView) itemView.findViewById(R.id.image_ico);
            title = (TextView) itemView.findViewById(R.id.title);
            address = (TextView) itemView.findViewById(R.id.address);
            license = (TextView) itemView.findViewById(R.id.license_number);
            workDate = (TextView) itemView.findViewById(R.id.work_day);
            newLogo = itemView.findViewById(R.id.image_new);
            location = itemView.findViewById(R.id.map);
            item = itemView.findViewById(R.id.item);
            mapClickListener = new MapClickListener();
            itemClickListener = new ItemClickListener();
            location.setOnClickListener(mapClickListener);
            item.setOnClickListener(itemClickListener);
        }
    }

    public class BannerHolder extends RecyclerView.ViewHolder {
        ImageView banner;
        BannerClickListener bannerClickListener;

        BannerHolder(View itemView) {
            super(itemView);
            banner = (ImageView) itemView.findViewById(R.id.banner);

            bannerClickListener = new BannerClickListener();
            banner.setOnClickListener(bannerClickListener);
        }
    }


    private class BannerClickListener implements View.OnClickListener {
        private AdItem mItem;

        @Override
        public void onClick(View v) {
            mClickInterface.bannerClicked(mItem);
        }

        public void setPosition(AdItem item) {
            mItem = item;
        }
    }


    private class MapClickListener implements View.OnClickListener {
        private LombardItem mLombardItem;

        @Override
        public void onClick(View v) {
            mClickInterface.mapClicked(mLombardItem);
        }

        public void setLombardItem(LombardItem lombardItem) {
            mLombardItem = lombardItem;
        }
    }

    private class ItemClickListener implements View.OnClickListener {
        private LombardItem mLombardItem;

        @Override
        public void onClick(View v) {
            mClickInterface.itemClicked(mLombardItem);
        }

        public void setLombardItem(LombardItem lombardItem) {
            mLombardItem = lombardItem;
        }
    }
}

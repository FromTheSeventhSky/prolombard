package com.fromtheseventhsky.lombards.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.models.DrawerItem;
import com.fromtheseventhsky.lombards.utils.Utils;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class DrawerAdapter  extends BaseAdapter {
    private ArrayList<DrawerItem> mItems;
    LayoutInflater mInflater;
    private Context mContext;
    private int mSelectedPosition;

    public DrawerAdapter(Context context, ArrayList<DrawerItem> items) {
        mItems = items;
        mContext = context;
        mSelectedPosition = -1;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return mItems.size();
    }

    @Override
    public Object getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void setSelectedPosition(int selectedPosition) {
        mSelectedPosition = selectedPosition;
        notifyDataSetChanged();
    }

    public int getSelectedPosition() {
        return mSelectedPosition;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;


        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item_drawer, parent, false);

            viewHolder = new ViewHolder();
            viewHolder.mImageViewIcon = (ImageView) convertView.findViewById(R.id.image_view_nav_drawer);
            viewHolder.mTextViewItem = (TextView) convertView.findViewById(R.id.text_view_nav_drawer);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        DrawerItem drawerItem = mItems.get(position);

        if(mSelectedPosition==position){
            viewHolder.mTextViewItem.setTextColor(Utils.getColor(mContext, R.color.white));
        } else {
            viewHolder.mTextViewItem.setTextColor(Utils.getColor(mContext, R.color.drawer_color_text));
        }
        viewHolder.mImageViewIcon.setImageResource(drawerItem.getIcon());
        viewHolder.mTextViewItem.setText(drawerItem.getItemTitle());


        return convertView;
    }

    private static final class ViewHolder {
        private TextView mTextViewItem;
        private ImageView mImageViewIcon;
    }

}

package com.fromtheseventhsky.lombards.adapters;

import android.animation.Animator;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bartoszlipinski.viewpropertyobjectanimator.ViewPropertyObjectAnimator;
import com.fromtheseventhsky.lombards.LombardApp;
import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.constants.ApiConst;
import com.fromtheseventhsky.lombards.models.AdItem;
import com.fromtheseventhsky.lombards.models.LombardItem;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class NotificationsAdapter extends BaseAdapter {
    private Context mContext;

    private boolean[] mOpened;

    private String shortStr, longStr;

    private int mHeight;

    private List<Object> mObjects;
    private ClickInterface mClickInterface;


    private LombardFilter mFilter;


    public NotificationsAdapter(Context context, ArrayList<Object> objects, ClickInterface clickInterface) {
        mContext = context;
        mClickInterface = clickInterface;
        mObjects = objects;
        mHeight = -1;
    }

    public void addAll(ArrayList<Object> objects) {
        if (objects == null) {
            return;
        }
        mObjects.clear();
        mObjects.addAll(objects);
        notifyDataSetChanged();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v;
        switch (viewType) {
            case BANNER:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_banner, parent, false);
                BannerHolder bannerHolder = new BannerHolder(v);
                return bannerHolder;
            case LIST_ITEM:
                v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_notification, parent, false);
                ListItemHolder listItemHolder = new ListItemHolder(v);
                return listItemHolder;
            default:
                return null;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)) {
            case BANNER: {
                BannerHolder bannerHolder = (BannerHolder) holder;
                AdItem item = (AdItem) mObjects.get(position);
                bannerHolder.bannerClickListener.setPosition(item);
                Picasso.with(LombardApp.sAppContext).load(item.getSmallUrl()).into(bannerHolder.banner);
            }
                break;
            case LIST_ITEM:
                ListItemHolder listItemHolder = (ListItemHolder) holder;
                LombardItem item = (LombardItem) mObjects.get(position);
                listItemHolder.title.setText(item.getLombardName());
                listItemHolder.address.setText(item.getLombardAddress());
                listItemHolder.notificationText.getLayoutParams().height = mHeight;
                listItemHolder.notificationText.setMaxLines(2);
                listItemHolder.notificationText.requestLayout();
                listItemHolder.itemClickListener.setLombardItem(item);
                listItemHolder.mapClickListener.setLombardItem(item);
                if(!item.getIco().equalsIgnoreCase("null")){
                    Picasso.with(LombardApp.sAppContext).load(ApiConst.MAIN_URL+item.getIco()).into(listItemHolder.logo);
                }
                break;

        }
    }

    @Override
    public int getItemCount() {
        return mObjects.size();
    }

    @Override
    public Filter getFilter() {
        if (mFilter == null) {
            mFilter = new LombardFilter(mObjects);
        }
        return mFilter;
    }

    @Override
    void clearObj() {
        mObjects.clear();
    }

    @Override
    void addObj(Object obj) {
        mObjects.add(obj);
    }


    @Override
    public int getItemViewType(int position) {
        if (position == mObjects.size() - 1 || position == 0) {
            return BANNER;
        }
        return LIST_ITEM;
    }


    public class ListItemHolder extends RecyclerView.ViewHolder {
        ImageView logo;
        TextView title, address, notificationText;
        View newLogo, location, item;
        MapClickListener mapClickListener;
        ItemClickListener itemClickListener;

        ListItemHolder(View itemView) {
            super(itemView);
            if (mHeight == -1) {
                mHeight = getHeight("1") * 2;
            }
            logo = (ImageView) itemView.findViewById(R.id.image_ico);
            title = (TextView) itemView.findViewById(R.id.title);
            address = (TextView) itemView.findViewById(R.id.address);
            notificationText = (TextView) itemView.findViewById(R.id.notification_text);
            newLogo = itemView.findViewById(R.id.image_new);
            location = itemView.findViewById(R.id.map);
            item = itemView.findViewById(R.id.item);
            mapClickListener = new MapClickListener();
            itemClickListener = new ItemClickListener();
            location.setOnClickListener(mapClickListener);
            item.setOnClickListener(itemClickListener);
            notificationText.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    toggleAnimation(v);
                }
            });
        }
    }

    public class BannerHolder extends RecyclerView.ViewHolder {
        ImageView banner;
        BannerClickListener bannerClickListener;

        BannerHolder(View itemView) {
            super(itemView);
            banner = (ImageView) itemView.findViewById(R.id.banner);

            bannerClickListener = new BannerClickListener();
            banner.setOnClickListener(bannerClickListener);
        }
    }


    private class BannerClickListener implements View.OnClickListener {
        private AdItem mItem;

        @Override
        public void onClick(View v) {
            mClickInterface.bannerClicked(mItem);
        }

        public void setPosition(AdItem item) {
            mItem = item;
        }
    }


    private class MapClickListener implements View.OnClickListener {
        private LombardItem mLombardItem;

        @Override
        public void onClick(View v) {
            mClickInterface.mapClicked(mLombardItem);
        }

        public void setLombardItem(LombardItem lombardItem) {
            mLombardItem = lombardItem;
        }
    }

    private class ItemClickListener implements View.OnClickListener {
        private LombardItem mLombardItem;

        @Override
        public void onClick(View v) {
            mClickInterface.itemClicked(mLombardItem);
        }

        public void setLombardItem(LombardItem lombardItem) {
            mLombardItem = lombardItem;
        }
    }

    private void toggleAnimation(View v) {
        if (v.getTag() == null) {
            openAnimation(v);
            return;
        }
        if ((boolean) v.getTag()) {
            closeAnimation(v);
        } else {
            openAnimation(v);
        }
    }

    private void openAnimation(final View v) {
        v.setTag(true);
        ViewPropertyObjectAnimator.animate(v)
                .height(getHeight(((TextView) v).getText()))
                .setDuration(300)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        ((TextView) v).setMaxLines(2000);
                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    private void closeAnimation(final View v) {
        v.setTag(false);
        ViewPropertyObjectAnimator.animate(v)
                .height(mHeight)
                .setDuration(300)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ((TextView) v).setMaxLines(2);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }


    private int getHeight(CharSequence text) {

        WindowManager wm =
                (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();

        int deviceWidth;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            Point size = new Point();
            display.getSize(size);
            deviceWidth = size.x;
        } else {
            deviceWidth = display.getWidth();
        }

        TextView textView = new TextView(mContext);
        int padding = mContext.getResources().getDimensionPixelSize(R.dimen.list_item_padding);
        textView.setPadding(padding, 0, padding, 0);
        textView.setText(text, TextView.BufferType.SPANNABLE);
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
        int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        textView.measure(widthMeasureSpec, heightMeasureSpec);
        return textView.getMeasuredHeight();
    }


}

package com.fromtheseventhsky.lombards.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class FilterAdapter extends BaseAdapter {

    private Context mContext;
    private LayoutInflater mInflater;
    private ArrayList<String> mCities;
    private boolean[] mCheckedItems;


    public FilterAdapter(Context context, ArrayList<String> cities) {
        mContext = context;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mCities = cities;
        if(cities!=null) {
            int size = cities.size();
            mCheckedItems = new boolean[size];
            for (int i = 0; i < size; i++) {
                mCheckedItems[i] = false;
            }
        } else {
            mCheckedItems = new boolean[25];
            for (int i = 0; i < 25; i++) {
                mCheckedItems[i] = false;
            }
        }
    }
    public boolean isChecked(int position){
        return mCheckedItems[position];
    }
    public void toggle(int position){
        mCheckedItems[position] = !mCheckedItems[position];
    }

    public ArrayList<String> getCheckedCities(){
        ArrayList<String> ret = new ArrayList<>();
        for (int i = 0; i < mCheckedItems.length; i++) {
            if(mCheckedItems[i]){
                ret.add(mCities.get(i));
            }
        }
        return ret;
    }

    @Override
    public int getCount() {
        if(mCities!=null) {
            return mCities.size();
        } else {
            return 0;
        }
    }
    public void replaceCities(ArrayList<String> cities){
        mCities = cities;
        if(cities!=null) {
            int size = cities.size();
            mCheckedItems = new boolean[size];
            for (int i = 0; i < size; i++) {
                mCheckedItems[i] = false;
            }
        }
    }

    @Override
    public String getItem(int position) {
        return mCities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.filter_item, parent, false);
            holder = new ViewHolder();
            holder.box = (CheckBox) convertView.findViewById(R.id.city_chb);
            holder.text = (TextView) convertView.findViewById(R.id.city_text);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.box.setChecked(mCheckedItems[position]);
        holder.text.setText(mCities.get(position));

        return convertView;
    }


    public class ViewHolder {
        public CheckBox box;
        public TextView text;
    }
}

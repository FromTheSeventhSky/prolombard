package com.fromtheseventhsky.lombards.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class DocumentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private String[] mDocuments;


    public DocumentsAdapter(String objects) {
        mDocuments = objects.split(",");
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_rates, parent, false);
        return new ListItemHolder(v);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ListItemHolder listItemHolder = (ListItemHolder) holder;
        listItemHolder.title.setText(mDocuments[position]);
        listItemHolder.value.setText("");
        if (position % 2 == 1) {
            listItemHolder.root.setBackgroundColor(listItemHolder.root.getResources().getColor(R.color.list_item_top_color));
        } else {
            listItemHolder.root.setBackgroundColor(listItemHolder.root.getResources().getColor(R.color.transparent));
        }
    }

    @Override
    public int getItemCount() {
        return mDocuments.length;
    }

    public class ListItemHolder extends RecyclerView.ViewHolder {
        LinearLayout root;
        TextView title, value;

        ListItemHolder(View itemView) {
            super(itemView);
            root = (LinearLayout) itemView.findViewById(R.id.root);
            title = (TextView) itemView.findViewById(R.id.title);
            value = (TextView) itemView.findViewById(R.id.value);
        }
    }

}

package com.fromtheseventhsky.lombards.adapters;

import android.support.v7.widget.RecyclerView;
import android.widget.Filter;
import android.widget.Filterable;

import com.fromtheseventhsky.lombards.constants.IntConst;
import com.fromtheseventhsky.lombards.models.AdItem;
import com.fromtheseventhsky.lombards.models.LombardItem;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public abstract class BaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> implements IntConst, Filterable {
    public interface ClickInterface {
        void mapClicked(LombardItem lombardItem);
        void bannerClicked(AdItem item);
        void itemClicked(LombardItem lombardItem);
        void itemRemoved();
    }

    abstract void clearObj();
    abstract void addObj(Object obj);


    protected class LombardFilter extends Filter {

        private List<Object> mList;

        protected LombardFilter(List<Object> objects) {
            mList = new ArrayList<>();
            synchronized (this) {
                mList.addAll(objects);
            }
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            String searchStr = constraint.toString().toLowerCase();
            FilterResults results = new FilterResults();
            if (!searchStr.equals("") && searchStr.length() > 0) {
                ArrayList<Object> filter = new ArrayList<>();
                for (Object object : mList) {
                    if (object instanceof LombardItem) {
                        LombardItem flight = (LombardItem) object;
                        if(flight.getLombardName().toLowerCase().contains(searchStr.toLowerCase())){
                            filter.add(flight);
                        }
                    }
                }
                results.values = filter;
                results.count = filter.size();
            } else {
                synchronized (this) {
                    results.values = mList;
                    results.count = mList.size();
                }
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<Object> filtered = (ArrayList<Object>) results.values;
            notifyDataSetChanged();
            clearObj();
            for (Object aFiltered : filtered) {
                addObj(aFiltered);
            }
            notifyDataSetChanged();
        }

    }
}

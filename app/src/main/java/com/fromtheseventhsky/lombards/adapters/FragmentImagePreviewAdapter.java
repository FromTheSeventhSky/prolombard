package com.fromtheseventhsky.lombards.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class FragmentImagePreviewAdapter extends FragmentPagerAdapter {
    public FragmentImagePreviewAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        return null;
    }

    @Override
    public int getCount() {
        return 0;
    }
}

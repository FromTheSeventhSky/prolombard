package com.fromtheseventhsky.lombards.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.fragments.lombards.TabAboutUs;
import com.fromtheseventhsky.lombards.fragments.lombards.TabCredits;
import com.fromtheseventhsky.lombards.fragments.lombards.TabDocuments;
import com.fromtheseventhsky.lombards.fragments.lombards.TabGoldCosts;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LombardsDetailPagerAdapter extends FragmentPagerAdapter {

    private TabAboutUs.OpenMisc mOpenMisc;
    private String[] mTitles;
    public LombardsDetailPagerAdapter(FragmentManager fm, TabAboutUs.OpenMisc openMisc, Context context) {
        super(fm);
        mOpenMisc = openMisc;
        mTitles = context.getResources().getStringArray(R.array.tab_titles);
    }


    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return TabAboutUs.newInstance(mOpenMisc);
            case 1:
                return TabCredits.newInstance(mOpenMisc);
            case 2:
                return TabGoldCosts.newInstance(mOpenMisc);
            case 3:
                return TabDocuments.newInstance(mOpenMisc);
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mTitles[position];
    }

    @Override
    public int getCount() {
        return 4;
    }
}

package com.fromtheseventhsky.lombards.services;

import android.app.IntentService;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.fromtheseventhsky.lombards.constants.ApiConst;
import com.fromtheseventhsky.lombards.constants.IntConst;
import com.fromtheseventhsky.lombards.constants.StringConst;
import com.fromtheseventhsky.lombards.constants.TableColumns;
import com.fromtheseventhsky.lombards.managers.AppPreferenceManager;
import com.fromtheseventhsky.lombards.managers.DatabaseManager;
import com.fromtheseventhsky.lombards.managers.NetworkManager;
import com.fromtheseventhsky.lombards.managers.ParserManager;
import com.fromtheseventhsky.lombards.models.GoldItem;
import com.fromtheseventhsky.lombards.models.LoanInner;
import com.fromtheseventhsky.lombards.models.LoanRates;
import com.fromtheseventhsky.lombards.models.LombardItem;
import com.fromtheseventhsky.lombards.models.RegisterError;
import com.squareup.okhttp.Response;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class NetworkService extends IntentService implements StringConst, IntConst, ApiConst, TableColumns {

    private NetworkManager mNetworkManager;
    private DatabaseManager mDatabaseManager;
    private Intent mIntent;
    private String mBroadcastFilter;

    public NetworkService() {
        super("NetworkService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        mNetworkManager = NetworkManager.getInstance(this);
        mDatabaseManager = DatabaseManager.getInstance(this);
        mIntent = intent;
        mBroadcastFilter = intent.getStringExtra(NETWORK_SERVICE_ACTION);
        int action = intent.getIntExtra(SERVICE_ACTION, -1);
        switch (action) {
            case SERVICE_REGISTRATION:
                registerUser();
                break;
            case SERVICE_LOGIN:
                loginUser();
                break;
            case SERVICE_UNLOGIN:
                unloginUser();
                break;
            case SERVICE_GET_LOMBARD:
                getLombardsList();
                break;
        }
    }

    private void registerUser(){
        String[] args = mIntent.getStringArrayExtra(SERVICE_STR_ARR_BUNDLE);
        if(!mNetworkManager.isOnline()){
            sendErrorMessage(SERVICE_ERROR_NO_INTERNET);
        }
        sendMessage(SERVICE_RUNNING);
        String url = REGISTER_URL;
        Response response = mNetworkManager.getResponseForRegister(url, args);
        if(response==null){
            sendErrorMessage(SERVICE_ERROR_WHILE_REQUEST);
            return;
        }
        if(!response.isSuccessful()){
            if(response.code()==SERVICE_ERROR_IN_RECEIVED_DATA){
                ArrayList<RegisterError> errors = ParserManager.getErrorsRegister(mNetworkManager.getJSONString(response));
                Bundle b = new Bundle();
                b.putParcelableArrayList(SERVICE_ERROR_RESULT_MSG, errors);
                sendErrorMessage(response.code(), b);
            } else {
                sendErrorMessage(response.code());
            }
            return;
        }
        String[] signArgs = new String[2];
        signArgs[0]=args[3];
        signArgs[1]=args[1];

        if(!makeSigningRequest(signArgs)){
            return;
        }

        sendMessage(SERVICE_FINISH);
    }

    private void loginUser(){
        if(!mNetworkManager.isOnline()){
            sendErrorMessage(SERVICE_ERROR_NO_INTERNET);
        }
        String[] args = mIntent.getStringArrayExtra(SERVICE_STR_ARR_BUNDLE);
        sendMessage(SERVICE_RUNNING);
        if(!makeSigningRequest(args)){
            return;
        }

        sendMessage(SERVICE_FINISH);
    }


    private boolean makeSigningRequest(String[] args){
        String url = SIGN_URL;
        Response response = mNetworkManager.getResponseForSign(url, args);
        if(response==null){
            sendErrorMessage(SERVICE_ERROR_WHILE_REQUEST);
            return false;
        }
        if(!response.isSuccessful()){
            sendErrorMessage(response.code());
            return false;
        }
        String code = ParserManager.parseAuthCode(mNetworkManager.getJSONString(response));
        if(code!=null) {
            AppPreferenceManager.getInstance(this).saveAuthCode(code);
        }
        return true;
    }

    private void unloginUser(){
        if(!mNetworkManager.isOnline()){
            sendErrorMessage(SERVICE_ERROR_NO_INTERNET);
        }
        sendMessage(SERVICE_RUNNING);
        String url = SIGN_URL+"/"+AppPreferenceManager.getInstance(this).getAuthCode();
        Response response = mNetworkManager.getResponseForUnSign(url);
        if(response==null){
            sendErrorMessage(SERVICE_ERROR_WHILE_REQUEST);
            return;
        }
        if(!response.isSuccessful()){
            sendErrorMessage(response.code());
            return;
        }
        AppPreferenceManager.getInstance(this).saveAuthCode("");

        sendMessage(SERVICE_FINISH);
    }


    private void testService(){
        sendMessage(SERVICE_RUNNING);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
            sendErrorMessage(0);
            return;
        }
        sendMessage(SERVICE_FINISH);
    }


    private void getLombardsList(){
        if(!mNetworkManager.isOnline()){
            sendErrorMessage(SERVICE_ERROR_NO_INTERNET);
        }
        sendMessage(SERVICE_RUNNING);

        Cursor c = mDatabaseManager.getAllRows(TABLE_LOMBARD_LIST, COLUMN_LOMBARD_CREATE_DATE, false);
        String url = "";
        if(c.moveToFirst()){
            url = GET_LOMBARDS_WITH_DATE + c.getString(c.getColumnIndex(COLUMN_LOMBARD_CREATE_DATE));
        } else {
            url = GET_LOMBARDS_WITHOUT_DATE;
        }
        c.close();
        Response response = mNetworkManager.getResponseForGetLombards(url);
        if(response==null){
            sendErrorMessage(SERVICE_ERROR_WHILE_REQUEST);
            return;
        }
        if(!response.isSuccessful()){
            sendErrorMessage(response.code());
            return;
        }
        String date = response.header("Date");
        try {
            date = URLEncoder.encode(date, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String json = mNetworkManager.getJSONString(response);
        ArrayList<LombardItem> lombardItems = ParserManager.parseLombards(json);
        if(lombardItems!=null) {
            if (lombardItems.size() > 0) {
                for (int i = 0; i < lombardItems.size(); i++) {
                    LombardItem li = lombardItems.get(i);
                    li.setUpdateDate(date);
                    long code = mDatabaseManager.insertRow(TABLE_LOMBARD_LIST, li.getContentValues());
                    ArrayList<LoanRates> loanItems = li.getLoanRates();
                    for (int j = 0; j < loanItems.size(); j++) {
                        LoanRates lr = loanItems.get(j);
                        mDatabaseManager.insertRow(TABLE_LOAN_LIST, lr.getContentValues());
                        ArrayList<LoanInner> loanInners = lr.getLoans();
                        for (int k = 0; k < loanInners.size(); k++) {
                            mDatabaseManager.insertRow(TABLE_LOAN_INNER_LIST, loanInners.get(k).getContentValues());
                        }
                    }
                    ArrayList<GoldItem> goldItems = li.getGoldRates();
                    for (int j = 0; j < goldItems.size(); j++) {
                        mDatabaseManager.insertRow(TABLE_GOLD, goldItems.get(j).getContentValues());
                    }
                }
            }
        } else {
            sendErrorMessage(SERVICE_ERROR_IN_RECEIVED_DATA);
            return;
        }
        AppPreferenceManager.getInstance(this).saveLombardUpdateDate(System.currentTimeMillis());
        sendMessage(SERVICE_FINISH);
    }


    private void sendMessage(int code) {
        Intent in = new Intent(mBroadcastFilter);
        in.putExtra(SERVICE_RESULT, code);
        LocalBroadcastManager.getInstance(this).sendBroadcast(in);
    }
    private void sendErrorMessage(int errorCode) {
        sendErrorMessage(errorCode, new Bundle());
    }
    private void sendErrorMessage(int errorCode, Bundle b) {
        Intent in = new Intent(mBroadcastFilter);
        in.putExtra(SERVICE_RESULT, SERVICE_ERROR);
        in.putExtra(SERVICE_ERROR_RESULT, errorCode);
        in.putExtra(SERVICE_ERROR_RESULT_MSG, b);
        LocalBroadcastManager.getInstance(this).sendBroadcast(in);
    }


}

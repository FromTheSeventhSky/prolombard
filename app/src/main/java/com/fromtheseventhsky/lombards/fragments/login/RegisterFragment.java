package com.fromtheseventhsky.lombards.fragments.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.fragments.abstracts.ResultReceiverFragment;
import com.fromtheseventhsky.lombards.models.RegisterError;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class RegisterFragment extends ResultReceiverFragment implements View.OnClickListener {

    private AppCompatEditText mLogin, mFio, mMail, mPhone, mPass, mPass2;

    private LinearLayout mLoadingRoot;
    private ScrollView mMainRoot;


    public static RegisterFragment newInstance() {
        return new RegisterFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.register_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mLogin = (AppCompatEditText) view.findViewById(R.id.login);
        mFio = (AppCompatEditText) view.findViewById(R.id.name);
        mMail = (AppCompatEditText) view.findViewById(R.id.email);
        mPhone = (AppCompatEditText) view.findViewById(R.id.telephone);
        mPass = (AppCompatEditText) view.findViewById(R.id.pass);
        mPass2 = (AppCompatEditText) view.findViewById(R.id.pass2);

        mLoadingRoot = (LinearLayout) view.findViewById(R.id.loadingLay);
        mMainRoot = (ScrollView) view.findViewById(R.id.registerLay);

        mPhone.addTextChangedListener(new TextWatcher() {

            int[] pattern = new int[]{2, 3, 2, 2};
            int[] patternWhitespace = new int[]{2, 6, 9};
            boolean erase;
            int length, cursorPos;
            String tmp;
            String first;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                erase = after == 0;
                first = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPhone.removeTextChangedListener(this);
                mPhone.setError(null);
                cursorPos = mPhone.getSelectionStart();

                tmp = s.toString().replaceAll("\\D", "");
                length = tmp.length();
                int i;
                for (i = 0; length > 0; i++) {
                    length -= pattern[i];
                }
                length = tmp.length();
                if (erase) {
                    if (cursorPos == 2 || cursorPos == 6 || cursorPos == 9) {
                        first = first.trim();
                        mPhone.setText(first);
                        mPhone.setSelection(cursorPos);
                    } else {
                        if (cursorPos == 3 || cursorPos == 7 || cursorPos == 10) {
                            cursorPos--;
                        }
                        for (int j = 1; j < i; j++) {
                            tmp = tmp.substring(0, patternWhitespace[j - 1]).concat(" ").concat(tmp.substring(patternWhitespace[j - 1], length));
                            length++;
                        }
                        mPhone.setText(tmp);
                        mPhone.setSelection(cursorPos);
                    }
                } else {
                    if (cursorPos == 3 || cursorPos == 7 || cursorPos == 10) {
                        cursorPos++;
                    }
                    for (int j = 1; j < i; j++) {
                        tmp = tmp.substring(0, patternWhitespace[j - 1]).concat(" ").concat(tmp.substring(patternWhitespace[j - 1], length));
                        length++;
                    }
                    mPhone.setText(tmp);
                    mPhone.setSelection(cursorPos);
                }

                mPhone.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        view.findViewById(R.id.enter).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.enter:
                if (mLogin.getText().toString().isEmpty()) {
                    mLogin.requestFocus();
                    mLogin.addTextChangedListener(new TextWatcher() {
                                                      @Override
                                                      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                      }

                                                      @Override
                                                      public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                      }

                                                      public void afterTextChanged(Editable edt) {
                                                          mLogin.setError(null);
                                                          mLogin.removeTextChangedListener(this);
                                                      }
                                                  }
                    );
                    mLogin.setError(getString(R.string.error_empty_login));
                    return;
                }
                if (!verifyLogin()) {
                    mLogin.requestFocus();
                    mLogin.addTextChangedListener(new TextWatcher() {
                                                      @Override
                                                      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                      }

                                                      @Override
                                                      public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                      }

                                                      public void afterTextChanged(Editable edt) {
                                                          mLogin.setError(null);
                                                          mLogin.removeTextChangedListener(this);
                                                      }
                                                  }
                    );
                    mLogin.setError(getString(R.string.error_wrong_login));
                    return;
                }

                if (mFio.getText().toString().isEmpty()) {
                    mFio.requestFocus();
                    mFio.addTextChangedListener(new TextWatcher() {
                                                    @Override
                                                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                    }

                                                    @Override
                                                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                    }

                                                    public void afterTextChanged(Editable edt) {
                                                        mFio.setError(null);
                                                        mFio.removeTextChangedListener(this);
                                                    }
                                                }
                    );
                    mFio.setError(getString(R.string.error_empty_login));
                    return;
                }
                if (mPhone.getText().toString().isEmpty()) {
                    mPhone.requestFocus();
                    mPhone.setError(getString(R.string.error_empty_login));
                    return;
                }
                if (!verifyPhone()) {
                    mPhone.requestFocus();
                    mPhone.setError(getString(R.string.error_phone));
                    return;
                }
                if (mMail.getText().toString().isEmpty()) {
                    mMail.requestFocus();
                    mMail.addTextChangedListener(new TextWatcher() {
                                                     @Override
                                                     public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                     }

                                                     @Override
                                                     public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                     }

                                                     public void afterTextChanged(Editable edt) {
                                                         mMail.setError(null);
                                                         mMail.removeTextChangedListener(this);
                                                     }
                                                 }
                    );
                    mMail.setError(getString(R.string.error_empty_login));
                    return;
                }
                if (!verifyEmail()) {
                    mMail.requestFocus();
                    mMail.addTextChangedListener(new TextWatcher() {
                                                     @Override
                                                     public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                     }

                                                     @Override
                                                     public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                     }

                                                     public void afterTextChanged(Editable edt) {
                                                         mMail.setError(null);
                                                         mMail.removeTextChangedListener(this);
                                                     }
                                                 }
                    );
                    mMail.setError(getString(R.string.error_email));
                    return;
                }

                if (mPass.getText().toString().isEmpty()) {
                    mPass.requestFocus();
                    mPass.setError(getString(R.string.error_empty_login));
                    mPass.addTextChangedListener(new TextWatcher() {
                                                     @Override
                                                     public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                     }

                                                     @Override
                                                     public void onTextChanged(CharSequence s, int start, int before, int count) {

                                                     }

                                                     public void afterTextChanged(Editable edt) {
                                                         mPass.setError(null);
                                                         mPass.removeTextChangedListener(this);
                                                     }
                                                 }
                    );
                    return;
                }
                if (mPass.getText().toString().length() < 8) {
                    mPass.requestFocus();
                    mPass.setError(getString(R.string.error_short_pass));
                    mPass.addTextChangedListener(new TextWatcher() {
                                                     @Override
                                                     public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                     }

                                                     @Override
                                                     public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                     }

                                                     public void afterTextChanged(Editable edt) {
                                                         mPass.setError(null);
                                                         mPass.removeTextChangedListener(this);
                                                     }
                                                 }
                    );
                    return;
                }
                if (mPass2.getText().toString().isEmpty()) {
                    mPass2.requestFocus();
                    mPass2.addTextChangedListener(new TextWatcher() {
                                                      @Override
                                                      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                      }

                                                      @Override
                                                      public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                      }

                                                      public void afterTextChanged(Editable edt) {
                                                          mPass2.setError(null);
                                                          mPass2.removeTextChangedListener(this);
                                                      }
                                                  }
                    );
                    mPass2.setError(getString(R.string.error_empty_login));
                    return;
                }

                if (!verifyPass()) {
                    mPass2.requestFocus();
                    mPass2.addTextChangedListener(new TextWatcher() {
                                                      @Override
                                                      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                                      }

                                                      @Override
                                                      public void onTextChanged(CharSequence s, int start, int before, int count) {
                                                      }

                                                      public void afterTextChanged(Editable edt) {
                                                          mPass2.setError(null);
                                                          mPass2.removeTextChangedListener(this);
                                                      }
                                                  }
                    );
                    mPass2.setError(getString(R.string.error_wrong_pass));
                    return;
                }

                String[] args = new String[6];
                args[0] = mMail.getText().toString().trim();
                args[1] = mPass.getText().toString().trim();
                args[2] = mPass2.getText().toString().trim();
                args[3] = mLogin.getText().toString().trim();
                args[4] = mFio.getText().toString().trim();
                args[5] = mPhone.getText().toString().trim();

                Intent intent = getIntent(SERVICE_REGISTRATION);
                intent.putExtra(SERVICE_STR_ARR_BUNDLE, args);
                getActivity().startService(intent);
                break;
        }
    }

    private void addTextWatcher(final EditText et){
        et.addTextChangedListener(new TextWatcher() {
                                         @Override
                                         public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                                         }

                                         @Override
                                         public void onTextChanged(CharSequence s, int start, int before, int count) {

                                         }

                                         public void afterTextChanged(Editable edt) {
                                             et.setError(null);
                                             et.removeTextChangedListener(this);
                                         }
                                     }
        );
    }

    private boolean verifyLogin() {
        String loginPattern = "^[a-zA-Z0-9]{0,}$";
        Pattern pattern = Pattern.compile(loginPattern);
        Matcher matcher = pattern.matcher(mLogin.getText().toString().trim());
        return matcher.matches();
    }

    private boolean verifyEmail() {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(mMail.getText().toString().trim()).matches();
    }

    private boolean verifyPass() {
        return mPass.getText().toString().equals(mPass2.getText().toString());
    }

    private boolean verifyPhone() {
        String phone = mPhone.getText().toString();
        phone = phone.replace(" ", "");
        return phone.length()==9;
    }


    @Override
    public void onServiceRunning() {
        mLoadingRoot.setVisibility(View.VISIBLE);
        mMainRoot.setVisibility(View.GONE);
    }

    @Override
    public void onServiceFinish() {
        mLoadingRoot.setVisibility(View.GONE);
        mMainRoot.setVisibility(View.VISIBLE);
        switchToMain();
    }

    @Override
    public void onServiceError(int errorCode, Bundle b) {
        mLoadingRoot.setVisibility(View.GONE);
        mMainRoot.setVisibility(View.VISIBLE);
        switch (errorCode){
            case SERVICE_ERROR_IN_RECEIVED_DATA:
                ArrayList<RegisterError> errors = b.getParcelableArrayList(SERVICE_ERROR_RESULT_MSG);
                if(errors!=null) {
                    for (int i = 0; i < errors.size(); i++) {
                        RegisterError re = errors.get(i);
                        switch (re.getKey()){
                            case "username":
                                switch (re.getValue()){
                                    case "has already been taken":
                                        mLogin.setError(getString(R.string.error_login_already_have));
                                        addTextWatcher(mLogin);
                                        break;
                                }
                                break;
                            case "email":
                                switch (re.getValue()){
                                    case "has already been taken":
                                        mMail.setError(getString(R.string.error_email_already_have));
                                        addTextWatcher(mMail);
                                        break;
                                }
                                break;
                            case "password_confirmation":
                                switch (re.getValue()){
                                    case "doesn't match Password":
                                        mPass2.setError(getString(R.string.error_pass));
                                        addTextWatcher(mPass2);
                                        break;
                                }
                                break;
                        }
                    }
                }
                break;
            case SERVICE_ERROR_NO_INTERNET:
                Snackbar.make(mMainRoot, R.string.error_no_internet, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_WHILE_REQUEST:
                Snackbar.make(mMainRoot, R.string.error_while_request, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_UNAUTHORISED:
                Snackbar.make(mMainRoot, R.string.error_unauthorised, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_BACKEND_CODE_ERROR:
                Snackbar.make(mMainRoot, R.string.error_backend, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_NGNIX_DOWN:
                Snackbar.make(mMainRoot, R.string.error_ngnix, Snackbar.LENGTH_LONG).show();
                break;
        }


    }

    @Override
    public String getBroadcastFilter() {
        return this.getClass().getName();
    }
}

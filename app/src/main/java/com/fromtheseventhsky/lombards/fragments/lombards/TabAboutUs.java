package com.fromtheseventhsky.lombards.fragments.lombards;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.activities.MainActivity;
import com.fromtheseventhsky.lombards.constants.ApiConst;
import com.fromtheseventhsky.lombards.constants.IntConst;
import com.fromtheseventhsky.lombards.models.LombardItem;
import com.fromtheseventhsky.lombards.utils.Utils;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class TabAboutUs extends BaseTabFragment implements View.OnClickListener, IntConst {

    private MainActivity mActivity;
    private OpenMisc mOpenMisc;
    private ArrayList<Bitmap> mBmpArray;

    private LombardItem mLombardItem;

    private ImageView[] mImViews;

    private Handler mHandler;
    private CharSequence[] mNumbers;

    private int mSelectedPosition;

    private View mImgNew;
    private ImageView mLogo, mCall;
    private TextView mTitle, mDesc, mLicense, mWorkTime, mPhone, mAddress, mSite, mEmail;
    private FrameLayout mMap;
    private HorizontalScrollView mPhotosRoot;

    public static TabAboutUs newInstance(OpenMisc openMisc) {
        TabAboutUs fragment = new TabAboutUs();
        fragment.setOpenMisc(openMisc);
        return fragment;
    }

    private void setOpenMisc(OpenMisc openMisc) {
        mOpenMisc = openMisc;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lombard_detail_about, container, false);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (mOpenMisc != null) {
            mLombardItem = mOpenMisc.getLombardItem();
        }

        //HandlerManager.getInstance().registerHandler(mHandler);
        //HandlerManager.getInstance().addToQuery("ddd", null);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        //HandlerManager.getInstance().unregisterHandler();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mImgNew = view.findViewById(R.id.image_new);
        mImgNew.setVisibility(View.GONE);
        mLogo = (ImageView) view.findViewById(R.id.image_ico);
        mTitle = (TextView) view.findViewById(R.id.title);
        mDesc = (TextView) view.findViewById(R.id.description);
        mLicense = (TextView) view.findViewById(R.id.license_number);
        mWorkTime = (TextView) view.findViewById(R.id.work_time);
        mPhone = (TextView) view.findViewById(R.id.phone);
        mAddress = (TextView) view.findViewById(R.id.address);
        mSite = (TextView) view.findViewById(R.id.site);
        mEmail = (TextView) view.findViewById(R.id.email);

        mCall = (ImageView) view.findViewById(R.id.callBtn);
        mMap = (FrameLayout) view.findViewById(R.id.mapContainer);

        mPhotosRoot = (HorizontalScrollView) view.findViewById(R.id.horizontalScrollView);

        mCall.setOnClickListener(this);
        mMap.setOnClickListener(this);

        if(mLombardItem==null){
            getActivity().onBackPressed();
            return;
        }
        if (!mLombardItem.getIco().equalsIgnoreCase("null")) {
            Picasso.with(getActivity()).load(ApiConst.MAIN_URL + mLombardItem.getIco()).into(mLogo);
        }

        String phones = mLombardItem.getTelephones();
        mNumbers = phones.split(",");
        mTitle.setText(mLombardItem.getLombardName());
        mDesc.setText(mLombardItem.getLombardDescription());
        mLicense.setText(mLombardItem.getLicenseNumber());
        // TODO: 07.04.2016 worktime should be edited via parse, which is missing now
        mWorkTime.setText(
                Utils.parseWorkTime(mLombardItem.getWorkTime()));
        mPhone.setText(phones.replace(",", "\n"));
        mAddress.setText(mLombardItem.getLombardAddress());
        mSite.setText(mLombardItem.getSite());
        mEmail.setText(mLombardItem.getEmail());
        mActivity = (MainActivity) getActivity();

        // TODO: 10.04.2016 images are missing now
        mPhotosRoot.setVisibility(View.GONE);

        mBmpArray = new ArrayList<>();
        mImViews = new ImageView[3];

        mImViews[0] = (ImageView) view.findViewById(R.id.preview_1);
        mImViews[1] = (ImageView) view.findViewById(R.id.preview_2);
        mImViews[2] = (ImageView) view.findViewById(R.id.preview_3);

        mImViews[0].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenMisc.openImage(0, mBmpArray);
            }
        });
        mImViews[1].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenMisc.openImage(1, mBmpArray);
            }
        });
        mImViews[2].setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenMisc.openImage(2, mBmpArray);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.callBtn:
                AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                b.setTitle(R.string.telephone_dots);
                b.setItems(mNumbers, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        mSelectedPosition = which;
                        if (Build.VERSION.SDK_INT >= 23) {
                            if (ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.READ_CONTACTS)
                                    != PackageManager.PERMISSION_GRANTED) {

                                TabAboutUs.this.requestPermissions(new String[]{Manifest.permission.CALL_PHONE},
                                        PERMISSION_CALL_CODE);

                            } else {
                                callPhone();
                            }
                        } else {
                            callPhone();
                        }
                    }
                });
                b.show();
                break;
            case R.id.mapContainer:
                mOpenMisc.openMap();
                break;
        }
    }

    private void callPhone() {
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + mNumbers[mSelectedPosition]));

        startActivity(intent);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {

        callPhone();
        switch (requestCode) {
            case PERMISSION_CALL_CODE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    callPhone();
                } else {
                    Toast.makeText(getActivity(), "not", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
}

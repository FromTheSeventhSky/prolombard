package com.fromtheseventhsky.lombards.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.models.LombardItem;
import com.fromtheseventhsky.lombards.utils.Utils;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class MapInnerFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private SupportMapFragment mMapFragment;

    private boolean mMapLoaded, mMapAvailable;

    private ArrayList<LombardItem> mItems;
    private MapInterface mMapInterface;

    private View mError, mMapRoot;


    public static MapInnerFragment newInstance(MapInterface mapInterface) {
        MapInnerFragment fragment = new MapInnerFragment();
        fragment.setMapInterface(mapInterface);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.map_fragment_layout, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mMapFragment = SupportMapFragment.newInstance();

        mError = view.findViewById(R.id.map_error);
        mMapRoot = view.findViewById(R.id.map_root);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                if (!mMapLoaded) {
                    FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                    transaction.add(R.id.map_root, mMapFragment).commit();
                    if (GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity()) == ConnectionResult.SUCCESS) {
                        mMapFragment.getMapAsync(MapInnerFragment.this);
                        mMapAvailable = true;
                    } else {
                        mMapAvailable = false;
                        mMapRoot.setVisibility(View.GONE);
                        mError.setVisibility(View.VISIBLE);
                    }
                }
            }
        });
    }

    @Override
    public void onDestroyView() {
        //if (mMapFragment != null)
        //    getFragmentManager().beginTransaction().remove(mMapFragment).commit();
        super.onDestroyView();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(41.3082, 69.2598))
                .zoom(11)
                .build();
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.moveCamera(cameraUpdate);
        if (!mMapLoaded) {
            mMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
                @Override
                public void onMapLoaded() {
                    mMapLoaded = true;
                    fillMap();
                }
            });
        } else {
            fillMap();
        }
    }

    public void setMarkers(ArrayList<LombardItem> items) {
        mItems = items;
        if (mMapAvailable && mMapLoaded) {
            fillMap();
        }
    }

    private void fillMap() {
        if (mItems != null) {
            mMap.clear();
            for (int i = 0; i < mItems.size(); i++) {
                MarkerOptions mo = new MarkerOptions();
                mo.title(String.valueOf(i));
                // TODO: 09.04.2016 normal_coords
                String coordinate = mItems.get(i).getLombardCoordinates();
                if (!coordinate.isEmpty()) {
                    String[] coordinatesArr = coordinate.split(",");
                    mo.position(new LatLng(Double.valueOf(coordinatesArr[0]), Double.valueOf(coordinatesArr[1])));

                    IconGenerator iconFactory = new IconGenerator(getActivity());
                    iconFactory.setColor(Utils.getColor(getActivity(), R.color.list_thumb_color));
                    iconFactory.makeIcon(mItems.get(i).getLombardName());
                    mo.icon(BitmapDescriptorFactory.fromBitmap(iconFactory.makeIcon(makeCharSequence(mItems.get(i).getLombardName()))));
                    mMap.addMarker(mo);

                }
            }
            mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if (mMapInterface != null) {
                        LombardItem item = mItems.get(Integer.valueOf(marker.getTitle()));
                        mMapInterface.openInner(item);
                    }
                    return true;
                }
            });
        }
    }

    private CharSequence makeCharSequence(String text) {
        SpannableStringBuilder ssb = new SpannableStringBuilder(text);
        ssb.setSpan(new StyleSpan(Typeface.NORMAL), 0, text.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0, text.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        return ssb;
    }

    public void setMapInterface(MapInterface mapInterface) {
        this.mMapInterface = mapInterface;
    }

    public interface MapInterface {
        void openInner(LombardItem items);
    }

    private Bitmap writeTextOnDrawable(int drawableId, String text) {

        Bitmap bm = BitmapFactory.decodeResource(getResources(), drawableId)
                .copy(Bitmap.Config.ARGB_8888, true);

        bm = Bitmap.createScaledBitmap(bm, 87, 117, false);
        Typeface tf = Typeface.create("Helvetica", Typeface.NORMAL);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);
        paint.setColor(Color.BLACK);
        paint.setTypeface(tf);
        //paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(14);

        Rect textRect = new Rect();
        paint.getTextBounds(text, 0, text.length(), textRect);

        int width;
        if (bm.getWidth() > Utils.convertToPixels(getActivity(), textRect.width())) {
            width = bm.getWidth();
        } else {
            width = Utils.convertToPixels(getActivity(), textRect.width());
        }

        Bitmap bitmap = Bitmap.createBitmap(width, bm.getHeight() + Utils.convertToPixels(getActivity(), textRect.height() + 5), Bitmap.Config.ARGB_8888);
        bitmap.setDensity(DisplayMetrics.DENSITY_DEFAULT);
        Canvas canvas = new Canvas(bitmap);


        //Calculate the positions
        int xPos = 0;//(canvas.getWidth() / 2);     //-2 is for regulating the x position offset

        //"- ((paint.descent() + paint.ascent()) / 2)" is the distance from the baseline to the center.
        int yPos = textRect.height();//(int) ((canvas.getHeight())/2 - (canvas.getHeight())/8 ) ;

        canvas.drawText(text, xPos, yPos, paint);
        canvas.translate(0, textRect.height() + Utils.convertToPixels(getActivity(), 5));
        canvas.drawBitmap(bm, 0, 0, paint);
        return bitmap;
    }

}

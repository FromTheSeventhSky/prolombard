package com.fromtheseventhsky.lombards.fragments.lombards;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.activities.MainActivity;
import com.fromtheseventhsky.lombards.adapters.BaseAdapter;
import com.fromtheseventhsky.lombards.constants.IntConst;
import com.fromtheseventhsky.lombards.fragments.BannerDialogFragment;
import com.fromtheseventhsky.lombards.fragments.FilterCitiesFragment;
import com.fromtheseventhsky.lombards.fragments.MapInnerFragment;
import com.fromtheseventhsky.lombards.fragments.abstracts.ResultReceiverFragment;
import com.fromtheseventhsky.lombards.models.AdItem;
import com.fromtheseventhsky.lombards.models.LombardItem;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public abstract class ListFragment extends ResultReceiverFragment implements IntConst,
        BaseAdapter.ClickInterface,
        SearchView.OnQueryTextListener,
        FilterCitiesFragment.FilterInterface {

    private RecyclerView mListView;
    private LinearLayout mLoadingRoot;

    private FrameLayout mFragmentRoot;


    protected MainActivity mActivity;


    abstract RecyclerView.Adapter getAdapter();
    abstract int getSelection();
    abstract String getTitle();
    abstract int getLayout();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        View v = inflater.inflate(getLayout(), parent, false);
        mActivity = (MainActivity) getActivity();
        setHasOptionsMenu(true);
        return v;
    }

    public View getErrorRoot(){
        return mFragmentRoot;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mListView = (RecyclerView) view.findViewById(R.id.list);
        mFragmentRoot = (FrameLayout) view.findViewById(R.id.container_fragment);
        mLoadingRoot = (LinearLayout) view.findViewById(R.id.loadingLay);
        LinearLayoutManager llm = new LinearLayoutManager(getActivity());
        mListView.setLayoutManager(llm);
        mListView.setAdapter(getAdapter());
    }

    public void showLoad(){
        mListView.setVisibility(View.GONE);
        mLoadingRoot.setVisibility(View.VISIBLE);
    }

    public void hideLoad(){
        mListView.setVisibility(View.VISIBLE);
        mLoadingRoot.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mActivity.setTitle(getTitle());
        changeSelection(getSelection());
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.toolbar_menu, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        ((BaseAdapter)getAdapter()).getFilter().filter(newText);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_filter:
                if(getAdapter().getItemCount()>0) {
                    mActivity.lockDrawer();
                    mActivity.increaseInnerLevel();
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left)
                            .replace(R.id.container_fragment, FilterCitiesFragment.newInstance(this))
                            .addToBackStack("")
                            .commit();
                }
                break;
            case R.id.action_search:
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void lockMenu() {
        setHasOptionsMenu(false);
    }

    @Override
    public void unlockMenu() {
        if(getActivity()!=null) {
            mActivity.setTitle(getTitle());
        }
        setHasOptionsMenu(true);
    }

    @Override
    public void mapClicked(final LombardItem lombardItem) {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                MapInnerFragment mapInnerFragment = MapInnerFragment.newInstance(null);
                ArrayList<LombardItem> item = new ArrayList<LombardItem>();
                item.add(lombardItem);
                mapInnerFragment.setMarkers(item);
                mActivity.lockDrawer();
                mActivity.increaseInnerLevel();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.container, mapInnerFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("")
                        .commit();
            }
        });
    }

    @Override
    public void bannerClicked(AdItem item) {
        if (item == null) {
            return;
        }
        Bundle bundle = new Bundle();
        bundle.putString(BannerDialogFragment.ARGUMENT_IMAGE, item.getLargeItem());
        bundle.putString(BannerDialogFragment.ARGUMENT_URL, item.getUrl());
        BannerDialogFragment bannerDialogFragment = new BannerDialogFragment();
        bannerDialogFragment.setArguments(bundle);
        bannerDialogFragment.show(getFragmentManager(), "dialog-banner");
    }

    @Override
    public void itemClicked(LombardItem mLombardItem) {
        mActivity.lockDrawer();
        mActivity.increaseInnerLevel();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.container, LombardDetailFragment.newInstance(mLombardItem))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack("")
                .commit();
    }

}

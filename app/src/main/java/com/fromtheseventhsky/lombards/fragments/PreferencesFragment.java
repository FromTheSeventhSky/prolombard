package com.fromtheseventhsky.lombards.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.fragments.abstracts.ChangeSelectionFragment;
import com.fromtheseventhsky.lombards.models.PreferenceItem;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class PreferencesFragment extends ChangeSelectionFragment {

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.drawer_6);
        changeSelection(PREFERENCES);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.preferences, container, false);
        setHasOptionsMenu(false);
        return v;
    }

    private ArrayList<PreferenceItem> fillPreferences(){
        ArrayList<PreferenceItem> preferenceItems = new ArrayList<>();

        PreferenceItem preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(true);
        preferenceItem.setName(getString(R.string.preference_main));
        preferenceItems.add(preferenceItem);

        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(false);
        preferenceItem.setName(getString(R.string.preference_account));
        preferenceItem.setPreferenceNumber(PREFERENCE_ACCOUNT);
        preferenceItems.add(preferenceItem);

        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(true);
        preferenceItem.setName(getString(R.string.preference_notifications_title));
        preferenceItems.add(preferenceItem);


        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(false);
        preferenceItem.setCheckable(true);
        preferenceItem.setName(getString(R.string.preference_notifications));
        preferenceItem.setPreferenceNumber(PREFERENCE_NOTIFICATION);
        preferenceItems.add(preferenceItem);

        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(false);
        preferenceItem.setCheckable(true);
        preferenceItem.setName(getString(R.string.preference_notif_sound));
        preferenceItem.setPreferenceNumber(PREFERENCE_NOTIFICATION_SOUND);
        preferenceItems.add(preferenceItem);

        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(true);
        preferenceItem.setName(getString(R.string.preference_additional));
        preferenceItems.add(preferenceItem);

        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(false);
        preferenceItem.setName(getString(R.string.preference_rate));
        preferenceItem.setPreferenceNumber(PREFERENCE_RATE);
        preferenceItems.add(preferenceItem);

        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(false);
        preferenceItem.setName(getString(R.string.preference_about_proj));
        preferenceItem.setPreferenceNumber(PREFERENCE_ABOUT_APP);
        preferenceItems.add(preferenceItem);

        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(false);
        preferenceItem.setName(getString(R.string.preference_about_us));
        preferenceItem.setPreferenceNumber(PREFERENCE_ABOUT_US);
        preferenceItems.add(preferenceItem);

        preferenceItem = new PreferenceItem();
        preferenceItem.setIsCategory(false);
        preferenceItem.setName(getString(R.string.preference_app_version));
        preferenceItems.add(preferenceItem);

        return preferenceItems;
    }
}

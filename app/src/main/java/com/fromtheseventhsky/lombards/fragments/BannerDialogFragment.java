package com.fromtheseventhsky.lombards.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.constants.StringConst;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class BannerDialogFragment extends DialogFragment implements StringConst {


    private String mImage;
    private String mUrl;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle args = getArguments();
        if (args != null) {
            mImage = args.getString(ARGUMENT_IMAGE);
            mUrl = args.getString(ARGUMENT_URL);
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        if (!mImage.equals("")) {
            View view = View.inflate(getActivity(), R.layout.dialog_banner, null);
            final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progress_bar_dialog);
            final TextView textView = (TextView) view.findViewById(R.id.text_view_dialog);
            final ImageView imageView = (ImageView) view.findViewById(R.id.image_view_dialog);

            Picasso.with(getActivity()).load(mImage).into(imageView, new Callback() {
                @Override
                public void onSuccess() {
                    progressBar.setVisibility(View.GONE);
                    imageView.setVisibility(View.VISIBLE);
                }

                @Override
                public void onError() {
                    progressBar.setVisibility(View.GONE);
                    imageView.setVisibility(View.GONE);
                    textView.setVisibility(View.VISIBLE);
                }
            });

            builder.setView(view);
        }

        if (!mUrl.equals("")) {
            builder.setPositiveButton(R.string.dialog_banner_button_positive_url, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mUrl));
                    startActivity(intent);
                }
            });
        }


        builder.setNegativeButton(R.string.dialog_button_close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                Button btnPositive = alertDialog.getButton(Dialog.BUTTON_POSITIVE);
                btnPositive.setTextSize(12);

                Button btnNeutral = alertDialog.getButton(Dialog.BUTTON_NEUTRAL);
                btnNeutral.setTextSize(12);

                Button buttonNegative = alertDialog.getButton(DialogInterface.BUTTON_NEGATIVE);
                buttonNegative.setTextSize(12);
            }
        });
        return alertDialog;
    }


}

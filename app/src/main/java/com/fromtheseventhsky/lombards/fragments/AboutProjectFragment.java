package com.fromtheseventhsky.lombards.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.fragments.abstracts.ChangeSelectionFragment;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class AboutProjectFragment  extends ChangeSelectionFragment {


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.drawer_7);
        changeSelection(ABOUT);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.about_project, container, false);
    }
}
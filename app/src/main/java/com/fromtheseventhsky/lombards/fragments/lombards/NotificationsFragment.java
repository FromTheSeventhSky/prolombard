package com.fromtheseventhsky.lombards.fragments.lombards;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.adapters.NotificationsAdapter;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class NotificationsFragment extends ListFragment {


    private NotificationsAdapter mAdapter;
    private TextView mTextMiss;

    @Override
    RecyclerView.Adapter getAdapter() {
        if(mAdapter == null){
            mAdapter = new NotificationsAdapter(getActivity(), new ArrayList<>(), this);
        }
        return mAdapter;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mTextMiss = (TextView) view.findViewById(R.id.favourites);
        mTextMiss.setText(R.string.notifications_empty);

        mTextMiss.setVisibility(View.VISIBLE);

        super.onViewCreated(view, savedInstanceState);
    }


    @Override
    int getSelection() {
        return NOTIFICATIONS_LIST;
    }

    @Override
    String getTitle() {
        return getString(R.string.drawer_4);
    }

    @Override
    int getLayout() {
        return R.layout.lombards_fragment;
    }

    @Override
    public void onServiceRunning() {
        
    }

    @Override
    public void onServiceFinish() {

    }

    @Override
    public void onServiceError(int errorCode, Bundle bundle) {

    }

    @Override
    public String getBroadcastFilter() {
        return this.getClass().getName();
    }

    @Override
    public void performClick(ArrayList<String> cities) {
        
    }

    @Override
    public void itemRemoved() {

    }
}

package com.fromtheseventhsky.lombards.fragments.lombards;

import android.animation.ObjectAnimator;
import android.content.ContentValues;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.activities.MainActivity;
import com.fromtheseventhsky.lombards.adapters.LombardsDetailPagerAdapter;
import com.fromtheseventhsky.lombards.constants.TableColumns;
import com.fromtheseventhsky.lombards.fragments.ImagePreviewFragment;
import com.fromtheseventhsky.lombards.fragments.MapInnerFragment;
import com.fromtheseventhsky.lombards.managers.DatabaseManager;
import com.fromtheseventhsky.lombards.models.LombardItem;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LombardDetailFragment extends Fragment implements BaseTabFragment.OpenMisc, ImagePreviewFragment.ImageInterface {

    private boolean isFavour;

    private MainActivity mActivity;

    private TabLayout mTabs;

    private LombardItem mLombardItem;
    private LombardDetailInterface mInterface;

    public static LombardDetailFragment newInstance(LombardItem lombardItem) {
        LombardDetailFragment fragment = new LombardDetailFragment();
        fragment.setLombardItem(lombardItem);
        fragment.setInterface(null);
        return fragment;
    }

    public static LombardDetailFragment newInstance(LombardItem lombardItem, LombardDetailInterface lombardDetailInterface) {
        LombardDetailFragment fragment = new LombardDetailFragment();
        fragment.setLombardItem(lombardItem);
        fragment.setInterface(lombardDetailInterface);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        //setRetainInstance(true);
        mActivity = (MainActivity) getActivity();


        return inflater.inflate(R.layout.lombard_detail_root, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mTabs = (TabLayout) view.findViewById(R.id.tabs);
        ViewPager pager = (ViewPager) view.findViewById(R.id.pager);
        LombardsDetailPagerAdapter adapter = new LombardsDetailPagerAdapter(getChildFragmentManager(), this, getActivity());
        pager.setAdapter(adapter);
        mTabs.setupWithViewPager(pager);
    }

    public void hideTabs() {
        ObjectAnimator mAnimator = new ObjectAnimator();
        mAnimator.setTarget(mTabs);
        mAnimator.setProperty(View.Y);
        mAnimator.setFloatValues(mTabs.getY(), mTabs.getY() - mTabs.getHeight());
        mAnimator.setDuration(300);
        mAnimator.start();
    }

    public void showTabs() {
        ObjectAnimator mAnimator = new ObjectAnimator();
        mAnimator.setTarget(mTabs);
        mAnimator.setProperty(View.Y);
        mAnimator.setFloatValues(mTabs.getY(), mTabs.getY() + mTabs.getHeight());
        mAnimator.setDuration(300);
        mAnimator.start();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.drawer_1);
    }


    public void setInterface(LombardDetailInterface mInterface) {
        this.mInterface = mInterface;
    }

    public interface LombardDetailInterface {
        void informAboutClose();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (mInterface != null) {
            mInterface.informAboutClose();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.lombard_menu, menu);
        MenuItem item = menu.findItem(R.id.action_favourite);
        if (mLombardItem.isFavourite()) {
            item.setIcon(R.drawable.star_press);
        } else {
            item.setIcon(R.drawable.star);
        }
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_favourite:
                if (!mLombardItem.isFavourite()) {
                    item.setIcon(R.drawable.star_press);
                    mLombardItem.setFavourite(true);
                    ContentValues cv = new ContentValues();
                    cv.put(TableColumns.COLUMN_FAVOURITES_LOMBARD_ID, mLombardItem.getLombardId());
                    DatabaseManager.getInstance(getActivity()).insertRow(TableColumns.TABLE_FAVOURITES_LIST, cv);
                } else {
                    item.setIcon(R.drawable.star);
                    mLombardItem.setFavourite(false);
                    String whereFaw = TableColumns.COLUMN_FAVOURITES_LOMBARD_ID.concat(" = ?");
                    String[] whereArgsFaw = {mLombardItem.getLombardId()};
                    DatabaseManager.getInstance(getActivity()).deleteRow(TableColumns.TABLE_FAVOURITES_LIST, whereFaw, whereArgsFaw);
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public LombardItem getLombardItem() {
        return mLombardItem;
    }

    @Override
    public void openMap() {
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                MapInnerFragment mapInnerFragment = MapInnerFragment.newInstance(null);
                ArrayList<LombardItem> item = new ArrayList<LombardItem>();
                item.add(mLombardItem);
                mapInnerFragment.setMarkers(item);
                mActivity.increaseInnerLevel();
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.map_and_other_container, mapInnerFragment)
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .addToBackStack("")
                        .commit();
            }
        });
    }

    @Override
    public void openImage(int position, ArrayList<Bitmap> bmp) {
        mActivity.increaseInnerLevel();
        getFragmentManager()
                .beginTransaction()
                .replace(R.id.map_and_other_container, ImagePreviewFragment.newInstance(position, this))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack("")
                .commit();
    }

    @Override
    public void lockMenu() {
        setHasOptionsMenu(false);
    }

    @Override
    public void unlockMenu() {
        setHasOptionsMenu(true);
    }

    public void setLombardItem(LombardItem lombardItem) {
        mLombardItem = lombardItem;
    }
}

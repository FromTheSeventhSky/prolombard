package com.fromtheseventhsky.lombards.fragments.lombards;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.adapters.LoanRatesAdapter;
import com.fromtheseventhsky.lombards.models.LombardItem;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class TabCredits extends BaseTabFragment {

    private RecyclerView mRecyclerView;
    private LombardItem mLombardItem;
    private OpenMisc mOpenMisc;

    public static TabCredits newInstance(OpenMisc openMisc) {
        TabCredits fragment = new TabCredits();
        fragment.setOpenMisc(openMisc);
        return fragment;
    }

    private void setOpenMisc(OpenMisc openMisc) {
        mOpenMisc = openMisc;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(mOpenMisc!=null) {
            mLombardItem = mOpenMisc.getLombardItem();
        }

        //HandlerManager.getInstance().registerHandler(mHandler);
        //HandlerManager.getInstance().addToQuery("ddd", null);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lombard_detail_credits, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        LoanRatesAdapter adapter = new LoanRatesAdapter(mLombardItem.getLoanRates());
        mRecyclerView.setAdapter(adapter);
    }
}
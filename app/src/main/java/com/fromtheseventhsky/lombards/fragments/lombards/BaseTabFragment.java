package com.fromtheseventhsky.lombards.fragments.lombards;

import android.graphics.Bitmap;
import android.support.v4.app.Fragment;

import com.fromtheseventhsky.lombards.models.LombardItem;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public abstract class BaseTabFragment extends Fragment {
    public interface OpenMisc {
        LombardItem getLombardItem();
        void openMap();
        void openImage(int position, ArrayList<Bitmap> bmp);
    }
}

package com.fromtheseventhsky.lombards.fragments;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bruce.pickerview.popwindow.DatePickerPopWin;
import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.fragments.abstracts.ChangeSelectionFragment;
import com.fromtheseventhsky.lombards.utils.Utils;

import java.util.Calendar;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class AccountReturnAmountFragment extends ChangeSelectionFragment implements View.OnClickListener {

    private LinearLayout mCount, mReCount;
    private TextView mSelectDate, mFullCount, mCredit, mPercent, mPenalty;
    private AppCompatEditText mEnterCount, mPercentEntered;
    private String mTodayDate;
    private ObjectAnimator obj2, obj1, mRecountAnim1, mRecountAnim2;
    private int mCurMonth, mCurDay, mCurYear, mYearDay;
    private int mEnteredMonth, mEnteredDay, mEnteredYear, mEnteredYearDay;



    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.drawer_2);
        changeSelection(CABINET);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_fragment, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {

        mCount = (LinearLayout) view.findViewById(R.id.first_lay);
        mReCount = (LinearLayout) view.findViewById(R.id.second_lay);

        obj1 = new ObjectAnimator();
        obj1.setProperty(View.ALPHA);
        obj1.setFloatValues(1f, 0f);
        obj1.setDuration(400);
        obj1.setTarget(mCount);
        obj1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mReCount.setVisibility(View.VISIBLE);
                mCount.setVisibility(View.GONE);
                obj2.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        obj2 = new ObjectAnimator();
        obj2.setProperty(View.ALPHA);
        obj2.setFloatValues(0f, 1f);
        obj2.setDuration(400);
        obj2.setTarget(mReCount);

        mRecountAnim1 = new ObjectAnimator();
        mRecountAnim1.setProperty(View.ALPHA);
        mRecountAnim1.setFloatValues(1f, 0f);
        mRecountAnim1.setDuration(400);
        mRecountAnim1.setTarget(mReCount);
        mRecountAnim1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mReCount.setVisibility(View.GONE);
                mCount.setVisibility(View.VISIBLE);
                mRecountAnim2.start();
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mRecountAnim2 = new ObjectAnimator();
        mRecountAnim2.setProperty(View.ALPHA);
        mRecountAnim2.setFloatValues(0f, 1f);
        mRecountAnim2.setDuration(400);
        mRecountAnim2.setTarget(mCount);


        Calendar calendar = Calendar.getInstance();

        mCurMonth = calendar.get(Calendar.MONTH) + 1;
        mCurDay = calendar.get(Calendar.DAY_OF_MONTH);
        mCurYear = calendar.get(Calendar.YEAR);
        mYearDay = calendar.get(Calendar.DAY_OF_YEAR);

        mTodayDate = String.valueOf(mCurYear)
                .concat("-")
                .concat(String.format("%02d", mCurMonth))
                .concat("-")
                .concat(String.format("%02d", mCurDay));


        mSelectDate = (TextView) view.findViewById(R.id.date_pick);
        mFullCount = (TextView) view.findViewById(R.id.full_count);
        mCredit = (TextView) view.findViewById(R.id.credit_count);
        mPercent = (TextView) view.findViewById(R.id.percent_count);
        mPenalty = (TextView) view.findViewById(R.id.penalty_count);

        mPercentEntered = (AppCompatEditText) view.findViewById(R.id.percent);

        mEnterCount = (AppCompatEditText) view.findViewById(R.id.credit);

        mSelectDate.setOnClickListener(this);
        view.findViewById(R.id.count).setOnClickListener(this);
        view.findViewById(R.id.recount).setOnClickListener(this);


    }

    private void countResult() {
        Calendar calendar = Calendar.getInstance();
        calendar.set(mEnteredYear, mEnteredMonth, mEnteredDay);
        if (mEnteredYear < mCurYear) {
            mYearDay += calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
        }
        int needDay = mYearDay - mEnteredYearDay;
        int penaltyDay = needDay - 30;
        long enteredSum = Long.valueOf(mEnterCount.getText().toString());
        double enteredPercent = Double.valueOf(mPercentEntered.getText().toString())/100;
        mSelectDate.setText("");
        mEnterCount.setText("");
        mTodayDate = String.valueOf(mCurYear)
                .concat("-")
                .concat(String.format("%02d", mCurMonth))
                .concat("-")
                .concat(String.format("%02d", mCurDay));
        double percent = 0;
        double penalty;
        double full;
        if (penaltyDay < 0) {
            penalty = 0;
            percent = enteredPercent * enteredSum;
            percent *= needDay;
            percent = ((long) (percent + 50) / 100) * 100;
        } else {
            percent = 30 * enteredSum;
            percent *= enteredPercent;
            percent = ((long) (percent + 50) / 100) * 100;
            penalty = 0.007d * (enteredSum + percent);
            penalty *= penaltyDay;
            penalty = ((long) (penalty + 50) / 100) * 100;
        }
        full = percent + penalty + enteredSum;

        mFullCount.setText(String.valueOf((long) full).concat(" ").concat(getString(R.string.currency)));
        mCredit.setText(String.valueOf((long) enteredSum).concat(" ").concat(getString(R.string.currency)));
        mPercent.setText(String.valueOf((long) percent).concat(" ").concat(getString(R.string.currency)));
        mPenalty.setText(String.valueOf((long) penalty).concat(" ").concat(getString(R.string.currency)));

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.date_pick:
                Utils.hideKeyboard(getActivity());
                new DatePickerPopWin.Builder(getActivity(), new DatePickerPopWin.OnDatePickedListener() {
                    @Override
                    public void onDatePickCompleted(int year, int month, int day, String dateDesc) {
                        mSelectDate.setText(dateDesc);
                        mEnteredMonth = month - 1;
                        mEnteredDay = day;
                        mEnteredYear = year;
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(mEnteredYear, mEnteredMonth, mEnteredDay);
                        mEnteredYearDay = calendar.get(Calendar.DAY_OF_YEAR);
                        mTodayDate = dateDesc;
                    }
                }).textConfirm("ПРИНЯТЬ") //text of confirm button
                        .textCancel("ОТМЕНИТЬ") //text of cancel button
                        .btnTextSize(16) // button text size
                        .viewTextSize(25) // pick view text size
                        .colorCancel(Color.parseColor("#999999")) //color of cancel button
                        .colorConfirm(Color.parseColor("#009900"))//color of confirm button
                        .minYear(1900) //min year in loop
                        .maxYear(2100) // max year in loop
                        .dateChose(mTodayDate) // date chose when init popwindow
                        .build().showPopWin(getActivity());
                break;
            case R.id.count:
                if (!mEnterCount.getText().toString().isEmpty() && !mSelectDate.getText().toString().isEmpty() && !mPercentEntered.getText().toString().isEmpty()) {
                    Calendar calendar = Calendar.getInstance();
                    long cur = calendar.getTimeInMillis();
                    calendar.set(mEnteredYear, mEnteredMonth, mEnteredDay, 0, 0, 0);
                    if(Double.valueOf(mPercentEntered.getText().toString())>10){
                        Toast.makeText(getActivity(), R.string.error_date, Toast.LENGTH_LONG).show();
                        break;
                    }
                    if (cur > calendar.getTimeInMillis()) {
                        Utils.hideKeyboard(getActivity());
                        obj1.start();
                        countResult();
                    } else {
                        Toast.makeText(getActivity(), R.string.error_date, Toast.LENGTH_LONG).show();
                        break;
                    }
                } else {
                    Toast.makeText(getActivity(), R.string.error_count, Toast.LENGTH_LONG).show();
                }
                break;
            case R.id.recount:
                mRecountAnim1.start();
                break;
        }
    }
}

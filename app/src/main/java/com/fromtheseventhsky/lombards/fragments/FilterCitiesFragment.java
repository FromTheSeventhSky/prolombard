package com.fromtheseventhsky.lombards.fragments;

import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.adapters.FilterAdapter;
import com.fromtheseventhsky.lombards.constants.TableColumns;
import com.fromtheseventhsky.lombards.managers.DatabaseManager;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class FilterCitiesFragment extends Fragment implements AdapterView.OnItemClickListener, TableColumns, View.OnClickListener {
    private ListView mList;
    private FilterInterface mFilterInterface;
    private FilterAdapter mFilterAdapter;
    public static FilterCitiesFragment newInstance(FilterInterface filterInterface) {
        FilterCitiesFragment fragment = new FilterCitiesFragment();
        fragment.setFilterInterface(filterInterface);
        return fragment;
    }

    private void setFilterInterface(FilterInterface filterInterface) {
        mFilterInterface = filterInterface;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.filter_layout, container, false);
        setHasOptionsMenu(true);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mList = (ListView) view.findViewById(R.id.filter_list);

        view.findViewById(R.id.enter).setOnClickListener(this);

        Cursor c = DatabaseManager.getInstance(getActivity()).getAllRows(TABLE_LOMBARD_LIST, new String[]{COLUMN_LOMBARD_REGION}, true);
        c.moveToFirst();
        ArrayList<String> list = new ArrayList<>();
        do {
            list.add(c.getString(c.getColumnIndex(COLUMN_LOMBARD_REGION)));
        } while (c.moveToNext());
        mFilterAdapter = new FilterAdapter(getActivity(), list);
        mList.setAdapter(mFilterAdapter);
        mList.setOnItemClickListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.filter);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFilterInterface.lockMenu();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mFilterInterface.unlockMenu();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mFilterAdapter.toggle(position);
        ((FilterAdapter.ViewHolder)view.getTag()).box.setChecked(mFilterAdapter.isChecked(position));
    }

    @Override
    public void onClick(View v) {
        ArrayList<String> ret = mFilterAdapter.getCheckedCities();
        if (ret.size()>0) {
            mFilterInterface.performClick(ret);
            getActivity().onBackPressed();
        } else {
            Toast.makeText(getActivity(), R.string.error_empty_city, Toast.LENGTH_LONG).show();
        }
    }

    public interface FilterInterface{
        void lockMenu();
        void unlockMenu();
        void performClick(ArrayList<String> cities);
    }

}

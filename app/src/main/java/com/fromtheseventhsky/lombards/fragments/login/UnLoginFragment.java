package com.fromtheseventhsky.lombards.fragments.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.activities.MainActivity;
import com.fromtheseventhsky.lombards.fragments.abstracts.ResultReceiverFragment;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class UnLoginFragment extends ResultReceiverFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_unlogin, container, false);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent intent = getIntent(SERVICE_UNLOGIN);
        getActivity().startService(intent);
    }

    @Override
    public void onServiceRunning() {

    }

    @Override
    public void onServiceFinish() {
        switchToLogin();
    }

    @Override
    public void onServiceError(int errorCode, Bundle bundle) {
        getFragmentManager().beginTransaction().remove(this).commit();
        switch (errorCode) {
            case SERVICE_ERROR_NO_INTERNET:
                Snackbar.make(((MainActivity)getActivity()).getActivityContent(), R.string.error_no_internet, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_WHILE_REQUEST:
                Snackbar.make(((MainActivity)getActivity()).getActivityContent(), R.string.error_while_request, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_UNAUTHORISED:
                Snackbar.make(((MainActivity)getActivity()).getActivityContent(), R.string.error_unauthorised, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_BACKEND_CODE_ERROR:
                Snackbar.make(((MainActivity)getActivity()).getActivityContent(), R.string.error_backend, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_NGNIX_DOWN:
                Snackbar.make(((MainActivity)getActivity()).getActivityContent(), R.string.error_ngnix, Snackbar.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public String getBroadcastFilter() {
        return this.getClass().getName();
    }
}

package com.fromtheseventhsky.lombards.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.activities.MainActivity;
import com.fromtheseventhsky.lombards.adapters.ImagePreviewAdapter;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class ImagePreviewFragment extends Fragment {

    private ImagePreviewAdapter mImagePreviewAdapter;
    private ViewPager mPager;
    private int mPrevPosition;

    private Handler mHandler;

    private ImageInterface mImageInterface;

    private static final String key = "position";

    public static ImagePreviewFragment newInstance(int position, ImageInterface imageInterface) {

        Bundle args = new Bundle();
        args.putInt(key, position);
        ImagePreviewFragment fragment = new ImagePreviewFragment();
        fragment.setArguments(args);
        fragment.setImageInterface(imageInterface);
        return fragment;
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.image_preview_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mImagePreviewAdapter = new ImagePreviewAdapter(getActivity());
        mPrevPosition = getArguments().getInt(key);
        mPager = (ViewPager)view.findViewById(R.id.image_preview_lay);
        mPager.setAdapter(mImagePreviewAdapter);
        mPager.setCurrentItem(mPrevPosition);
        mPager.setOffscreenPageLimit(5);
        mPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.photo_padding));
        mPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                mImagePreviewAdapter.setPrevPosition(mPrevPosition);
                mPrevPosition = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {
                mImagePreviewAdapter.resetScale();
            }
        });
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                if (msg.what == 1) {
                    mImagePreviewAdapter.setImage(0);
                }
            }
        };
        //HandlerManager.getInstance().registerInnerHandler(mHandler);

    }



    public void setImageInterface(ImageInterface imageInterface) {
        mImageInterface = imageInterface;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ((MainActivity)getActivity()).toggleColorToolbar(false);
        if(mImageInterface!=null) {
            mImageInterface.lockMenu();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        ((MainActivity)getActivity()).toggleColorToolbar(false);
    }

    @Override
    public void onStop() {
        super.onStop();
        ((MainActivity)getActivity()).toggleColorToolbar(true);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(mImageInterface!=null) {
            mImageInterface.unlockMenu();
        }
        //HandlerManager.getInstance().unregisterHandler();
    }

    public interface ImageInterface{
        void lockMenu();
        void unlockMenu();
    }

}

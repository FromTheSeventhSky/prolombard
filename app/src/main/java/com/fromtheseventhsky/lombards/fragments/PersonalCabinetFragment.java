package com.fromtheseventhsky.lombards.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.fragments.abstracts.ResultReceiverFragment;
import com.fromtheseventhsky.lombards.utils.Utils;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class PersonalCabinetFragment extends ResultReceiverFragment implements View.OnClickListener {

    private Spinner mCountry, mCities;
    private AppCompatEditText mLogin, mFio, mMail, mPhone, mPass, mPass2;

    private ImageView mMaleImg, mFemaleImg;
    private TextView mMaleText, mFemaleText;
    private boolean mIsMale;

    @Override
    public void onServiceRunning() {

    }

    @Override
    public void onServiceFinish() {

    }

    @Override
    public void onServiceError(int errorCode, Bundle bundle) {

    }

    @Override
    public String getBroadcastFilter() {
        return this.getClass().getName();
    }


    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.drawer_2);
        changeSelection(CABINET);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.personal_cabinet_fragment, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {

        mIsMale = true;
        mLogin = (AppCompatEditText) view.findViewById(R.id.login);
        mFio = (AppCompatEditText) view.findViewById(R.id.name);
        mMail = (AppCompatEditText) view.findViewById(R.id.email);
        mPhone = (AppCompatEditText) view.findViewById(R.id.telephone);
        mPass = (AppCompatEditText) view.findViewById(R.id.pass);
        mPass2 = (AppCompatEditText) view.findViewById(R.id.pass2);

        view.findViewById(R.id.male).setOnClickListener(this);
        view.findViewById(R.id.female).setOnClickListener(this);
        mMaleImg = (ImageView) view.findViewById(R.id.male_image);
        mFemaleImg = (ImageView) view.findViewById(R.id.female_image);
        mMaleText = (TextView) view.findViewById(R.id.male_text);
        mFemaleText = (TextView) view.findViewById(R.id.female_text);

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getActivity(),
                R.array.drawer_titles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        mCountry = ((Spinner) view.findViewById(R.id.spinner_countries));
        mCountry.setAdapter(adapter);

        mPhone.addTextChangedListener(new TextWatcher() {

            int[] pattern = new int[]{2, 3, 2, 2};
            int[] patternWhitespace = new int[]{2, 6, 9};
            boolean erase;
            int length, cursorPos;
            String tmp;
            String first;

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                erase = after == 0;
                first = s.toString();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mPhone.removeTextChangedListener(this);

                cursorPos = mPhone.getSelectionStart();

                tmp = s.toString().replaceAll("\\D", "");
                length = tmp.length();
                int i;
                for (i = 0; length > 0; i++) {
                    length -= pattern[i];
                }
                length = tmp.length();
                if (erase) {
                    if (cursorPos == 2 || cursorPos == 6 || cursorPos == 9) {
                        first = first.trim();
                        mPhone.setText(first);
                        mPhone.setSelection(cursorPos);
                    } else {
                        if (cursorPos == 3 || cursorPos == 7 || cursorPos == 10) {
                            cursorPos--;
                        }
                        for (int j = 1; j < i; j++) {
                            tmp = tmp.substring(0, patternWhitespace[j - 1]).concat(" ").concat(tmp.substring(patternWhitespace[j - 1], length));
                            length++;
                        }
                        mPhone.setText(tmp);
                        mPhone.setSelection(cursorPos);
                    }
                } else {
                    if (cursorPos == 3 || cursorPos == 7 || cursorPos == 10) {
                        cursorPos++;
                    }
                    for (int j = 1; j < i; j++) {
                        tmp = tmp.substring(0, patternWhitespace[j - 1]).concat(" ").concat(tmp.substring(patternWhitespace[j - 1], length));
                        length++;
                    }
                    mPhone.setText(tmp);
                    mPhone.setSelection(cursorPos);
                }

                mPhone.addTextChangedListener(this);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


        /*ViewTreeObserver vto = view.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    view.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                } else {
                    view.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            }
        });*/

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.male:
                mIsMale = true;
                mMaleImg.setImageResource(R.drawable.man);
                mFemaleImg.setImageResource(R.drawable.womanopasity);
                mMaleText.setTextColor(Utils.getColor(getActivity(), R.color.white));
                mFemaleText.setTextColor(Utils.getColor(getActivity(), R.color.white_transparent));
                break;
            case R.id.female:
                mIsMale = false;
                mMaleImg.setImageResource(R.drawable.manopasity);
                mFemaleImg.setImageResource(R.drawable.woman);
                mMaleText.setTextColor(Utils.getColor(getActivity(), R.color.white_transparent));
                mFemaleText.setTextColor(Utils.getColor(getActivity(), R.color.white));
                break;
        }
    }
}

package com.fromtheseventhsky.lombards.fragments.abstracts;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;

import com.fromtheseventhsky.lombards.LombardApp;
import com.fromtheseventhsky.lombards.services.NetworkService;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public abstract class ResultReceiverFragment extends ChangeSelectionFragment {

    protected int mResultCode;
    private BroadcastReceiver mBroadcastReceiver;

    public abstract void onServiceRunning();
    public abstract void onServiceFinish();
    public abstract void onServiceError(int errorCode, Bundle bundle);

    public abstract String getBroadcastFilter();


    public Intent getIntent(int type){
        Intent intent = new Intent(LombardApp.sAppContext, NetworkService.class);
        intent.setAction(getBroadcastFilter());
        intent.putExtra(SERVICE_ACTION, type);
        intent.putExtra(NETWORK_SERVICE_ACTION, getBroadcastFilter());
        return intent;
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                int resultCode = intent.getIntExtra(SERVICE_RESULT, SERVICE_ERROR);
                switch (resultCode) {
                    case SERVICE_RUNNING:
                        onServiceRunning();
                        break;
                    case SERVICE_FINISH:
                        onServiceFinish();
                        break;
                    case SERVICE_ERROR:
                        onServiceError(intent.getIntExtra(SERVICE_ERROR_RESULT, -1), intent.getBundleExtra(SERVICE_ERROR_RESULT_MSG));
                        break;
                }
            }
        };
    }

    @Override
    public void onResume() {
        super.onResume();
        IntentFilter filter = new IntentFilter(getBroadcastFilter());
        LocalBroadcastManager.getInstance(LombardApp.sAppContext).registerReceiver(mBroadcastReceiver, filter);
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(LombardApp.sAppContext).unregisterReceiver(mBroadcastReceiver);
    }
}

package com.fromtheseventhsky.lombards.fragments.login;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.AppCompatEditText;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.inputmethod.InputMethodManager;

import com.bartoszlipinski.viewpropertyobjectanimator.ViewPropertyObjectAnimator;
import com.fromtheseventhsky.lombards.LombardApp;
import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.fragments.abstracts.ResultReceiverFragment;
import com.fromtheseventhsky.lombards.managers.AppPreferenceManager;

/**
 * Created by FromTheSeventhSky in 2015.
 */
public class LoginFragment extends ResultReceiverFragment implements View.OnClickListener {

    private View mRoot;

    private boolean mAnimationShown;

    private View mFirstLogoRoot, mSecondLogoRoot, mContentRoot, mLoadingRoot;

    private AppCompatEditText mLogin, mPassword;

    public static LoginFragment newInstance() {
        return new LoginFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.login_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        mRoot = view.findViewById(R.id.content_root);
        mFirstLogoRoot = view.findViewById(R.id.logo_first_root);
        mSecondLogoRoot = view.findViewById(R.id.logo_second_root);

        mContentRoot = view.findViewById(R.id.mainRoot);
        mLoadingRoot = view.findViewById(R.id.loadingLay);

        mLogin = (AppCompatEditText) view.findViewById(R.id.login);
        mPassword = (AppCompatEditText) view.findViewById(R.id.pass);


        view.findViewById(R.id.enter).setOnClickListener(this);
        view.findViewById(R.id.register).setOnClickListener(this);
        view.findViewById(R.id.forgot_pass).setOnClickListener(this);

        if (!mAnimationShown) {
            mRoot.setVisibility(View.INVISIBLE);
            mSecondLogoRoot.setVisibility(View.VISIBLE);
            mFirstLogoRoot.setVisibility(View.INVISIBLE);
            String authCode = AppPreferenceManager.getInstance(LombardApp.sAppContext).getAuthCode();
            if (authCode.isEmpty()) {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        mAnimationShown = true;
                        mFirstLogoRoot.setVisibility(View.VISIBLE);
                        mSecondLogoRoot.setVisibility(View.GONE);
                        startNotAuthorisedAnimation();
                    }
                }, 1000);
            } else {
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        switchToMain();
                    }
                }, 1000);
            }
        } else {
            mFirstLogoRoot.setVisibility(View.VISIBLE);
            mSecondLogoRoot.setVisibility(View.GONE);
            mRoot.setVisibility(View.VISIBLE);
        }
    }




    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.background_img:
                ((InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE)).
                        hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                break;
            case R.id.register:
                getFragmentManager().beginTransaction()
                        .replace(R.id.register_lay, RegisterFragment.newInstance())
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack("RegisterFragment").commit();
                break;
            case R.id.forgot_pass:
                getFragmentManager().beginTransaction()
                        .replace(R.id.register_lay, ForgotPassFragment.newInstance())
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN).addToBackStack("ForgotPassFragment").commit();
                break;
            case R.id.enter:
                String[] args = new String[2];
                args[0] = mLogin.getText().toString().trim();
                args[1] = mPassword.getText().toString().trim();
                Intent intent = getIntent(SERVICE_LOGIN);
                intent.putExtra(SERVICE_STR_ARR_BUNDLE, args);
                getActivity().startService(intent);
                break;
        }
    }

    private void startNotAuthorisedAnimation() {
        int height = mRoot.getHeight();
        mRoot.getLayoutParams().height = 1;
        mRoot.requestLayout();
        //mRoot.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        ViewPropertyObjectAnimator.animate(mRoot)
                .height(height)
                .setDuration(1000)
                .setInterpolator(new AccelerateDecelerateInterpolator())
                .addListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animator animation) {
                        ObjectAnimator obj = new ObjectAnimator();
                        obj.setProperty(View.ALPHA);
                        obj.setFloatValues(0f, 1f);
                        obj.setDuration(500);
                        obj.setTarget(mRoot);
                        obj.start();
                        //mRoot.setLayerType(View.LAYER_TYPE_NONE, null);
                        mRoot.setVisibility(View.VISIBLE);
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {

                    }

                    @Override
                    public void onAnimationRepeat(Animator animation) {

                    }
                })
                .start();
    }

    @Override
    public void onServiceRunning() {
        mContentRoot.setVisibility(View.GONE);
        mLoadingRoot.setVisibility(View.VISIBLE);
    }

    @Override
    public void onServiceFinish() {
        switchToMain();
    }



    @Override
    public void onServiceError(int errorCode, Bundle bundle) {
        mContentRoot.setVisibility(View.VISIBLE);
        mLoadingRoot.setVisibility(View.GONE);
        switch (errorCode) {
            case SERVICE_ERROR_NO_INTERNET:
                Snackbar.make(mContentRoot, R.string.error_no_internet, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_WHILE_REQUEST:
                Snackbar.make(mContentRoot, R.string.error_while_request, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_UNAUTHORISED:
                Snackbar.make(mContentRoot, R.string.error_unauthorised, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_BACKEND_CODE_ERROR:
                Snackbar.make(mContentRoot, R.string.error_backend, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_NGNIX_DOWN:
                Snackbar.make(mContentRoot, R.string.error_ngnix, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_IN_RECEIVED_DATA:
                Snackbar.make(mContentRoot, R.string.error_in_received_data_login, Snackbar.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public String getBroadcastFilter() {
        return this.getClass().getName();
    }
}

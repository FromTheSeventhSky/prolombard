package com.fromtheseventhsky.lombards.fragments;

import android.animation.ObjectAnimator;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.activities.LoginActivity;
import com.fromtheseventhsky.lombards.activities.MainActivity;
import com.fromtheseventhsky.lombards.fragments.abstracts.ResultReceiverFragment;
import com.fromtheseventhsky.lombards.fragments.lombards.LombardDetailFragment;
import com.fromtheseventhsky.lombards.loaders.MapListLoader;
import com.fromtheseventhsky.lombards.managers.AppPreferenceManager;
import com.fromtheseventhsky.lombards.models.LombardItem;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class MapFragment extends ResultReceiverFragment implements MapInnerFragment.MapInterface,
        LoaderManager.LoaderCallbacks<ArrayList<LombardItem>>, LombardDetailFragment.LombardDetailInterface {


    private MapInnerFragment mMapInnerFragment;
    private View mMapRoot;

    protected MainActivity mActivity;

    @Override
    public void onServiceRunning() {

    }

    @Override
    public void onServiceFinish() {
        getLoaderManager().initLoader(1, null, this).forceLoad();
    }

    @Override
    public void onServiceError(int errorCode, Bundle bundle) {
        switch (errorCode) {
            case SERVICE_ERROR_NO_INTERNET:
                Snackbar.make(mMapRoot, R.string.error_no_internet, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_WHILE_REQUEST:
                Snackbar.make(mMapRoot, R.string.error_while_request, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_UNAUTHORISED:
                Toast.makeText(getActivity().getApplicationContext(), R.string.error_unauthorised, Toast.LENGTH_LONG).show();
                AppPreferenceManager.getInstance(getActivity()).saveAuthCode("");
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case SERVICE_ERROR_BACKEND_CODE_ERROR:
                Snackbar.make(mMapRoot, R.string.error_backend, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_NGNIX_DOWN:
                Snackbar.make(mMapRoot, R.string.error_ngnix, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_IN_RECEIVED_DATA:
                Snackbar.make(mMapRoot, R.string.error_in_data, Snackbar.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public String getBroadcastFilter() {
        return getClass().getName();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(R.string.drawer_3);
        changeSelection(LOMBARD_MAP);
        getLoaderManager().initLoader(1, null, this).forceLoad();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.lombard_map, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        mActivity = (MainActivity) getActivity();
        mMapRoot = view.findViewById(R.id.map_root);
        mMapInnerFragment = MapInnerFragment.newInstance(this);
        new Handler().post(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
                transaction.add(R.id.map_root, mMapInnerFragment).commit();
            }
        });
    }



    @Override
    public Loader<ArrayList<LombardItem>> onCreateLoader(int id, Bundle args) {
        return new MapListLoader(getActivity());
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<LombardItem>> loader, ArrayList<LombardItem> data) {
        if (data.size() > 0) {
            mMapInnerFragment.setMarkers(data);
        }
        if (System.currentTimeMillis() - AppPreferenceManager.getInstance(getActivity()).getLombardUpdateDate() > 1200000) {
            Intent intent = getIntent(SERVICE_GET_LOMBARD);
            getActivity().startService(intent);
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<LombardItem>> loader) {

    }

    @Override
    public void openInner(LombardItem item) {
        mActivity.lockDrawer();
        mActivity.increaseInnerLevel();

        ObjectAnimator obj = new ObjectAnimator();
        obj.setProperty(View.ALPHA);
        obj.setFloatValues(1f, 0f);
        obj.setDuration(300);
        obj.setTarget(mMapRoot);
        obj.start();

        getFragmentManager()
                .beginTransaction()
                .add(R.id.container, LombardDetailFragment.newInstance(item, this))
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .addToBackStack("")
                .commit();
    }


    @Override
    public void informAboutClose() {
        ObjectAnimator obj = new ObjectAnimator();
        obj.setProperty(View.ALPHA);
        obj.setFloatValues(0f, 1f);
        obj.setDuration(200);
        obj.setTarget(mMapRoot);
        obj.start();
    }
}
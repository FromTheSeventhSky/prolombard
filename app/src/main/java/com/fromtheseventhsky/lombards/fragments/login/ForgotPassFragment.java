package com.fromtheseventhsky.lombards.fragments.login;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fromtheseventhsky.lombards.R;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class ForgotPassFragment extends Fragment {


    public static ForgotPassFragment newInstance() {
        return new ForgotPassFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.forgot_pass_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

    }
}

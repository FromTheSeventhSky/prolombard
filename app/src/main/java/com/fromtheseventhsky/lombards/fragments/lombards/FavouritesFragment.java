package com.fromtheseventhsky.lombards.fragments.lombards;

import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.adapters.FavouritesAdapter;
import com.fromtheseventhsky.lombards.loaders.FavouritesListLoader;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */

public class FavouritesFragment extends ListFragment implements LoaderManager.LoaderCallbacks<ArrayList<Object>>  {

    private FavouritesAdapter mAdapter;

    private TextView mTextMiss;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mTextMiss = (TextView) view.findViewById(R.id.favourites);
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    RecyclerView.Adapter getAdapter() {
        if (mAdapter == null) {
            mAdapter = new FavouritesAdapter(new ArrayList<>(), this);
        }
        return mAdapter;
    }

    @Override
    int getSelection() {
        return FAVORITE;
    }

    @Override
    String getTitle() {
        return getString(R.string.drawer_5);
    }

    @Override
    int getLayout() {
        return R.layout.lombards_fragment;
    }


    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(1, null, this).forceLoad();
    }


    @Override
    public void onServiceRunning() {
    }

    @Override
    public void onServiceFinish() {

    }

    @Override
    public void onServiceError(int errorCode, Bundle bundle) {

    }

    @Override
    public String getBroadcastFilter() {
        return this.getClass().getName();
    }

    @Override
    public Loader<ArrayList<Object>> onCreateLoader(int id, Bundle args) {
        if(args!=null) {
            return new FavouritesListLoader(getActivity(), args.getStringArrayList(BUNDLE_ARR));
        } else {
            return new FavouritesListLoader(getActivity(), null);
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Object>> loader, ArrayList<Object> data) {
        if(data.size()>0) {
            mAdapter.addAll(data);
            mTextMiss.setVisibility(View.GONE);
        } else {
            mAdapter.addAll(data);
            mTextMiss.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Object>> loader) {

    }

    @Override
    public void performClick(ArrayList<String> cities) {
        getLoaderManager().destroyLoader(1);
        Bundle b = new Bundle();
        b.putStringArrayList(BUNDLE_ARR, cities);
        getLoaderManager().initLoader(1, b, this).forceLoad();
    }

    @Override
    public void itemRemoved() {
        mTextMiss.setVisibility(View.VISIBLE);
    }
}
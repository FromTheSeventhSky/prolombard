package com.fromtheseventhsky.lombards.fragments.lombards;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.activities.LoginActivity;
import com.fromtheseventhsky.lombards.adapters.LombardsAdapter;
import com.fromtheseventhsky.lombards.loaders.LombardListLoader;
import com.fromtheseventhsky.lombards.managers.AppPreferenceManager;

import java.util.ArrayList;

;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LombardListFragment extends ListFragment implements LoaderManager.LoaderCallbacks<ArrayList<Object>> {

    private LombardsAdapter mAdapter;

    @Override
    RecyclerView.Adapter getAdapter() {
        if (mAdapter == null) {
            mAdapter = new LombardsAdapter(new ArrayList<>(), this);
        }
        return mAdapter;
    }

    @Override
    int getSelection() {
        return LOMBARD_LIST;
    }

    @Override
    String getTitle() {
        return getString(R.string.drawer_1);
    }

    @Override
    int getLayout() {
        return R.layout.lombards_fragment;
    }

    @Override
    public void onServiceRunning() {
        showLoad();
    }

    @Override
    public void onServiceFinish() {
        getLoaderManager().initLoader(1, null, this).forceLoad();
    }

    @Override
    public void onServiceError(int errorCode, Bundle bundle) {
        switch (errorCode) {
            case SERVICE_ERROR_NO_INTERNET:
                Snackbar.make(getErrorRoot(), R.string.error_no_internet, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_WHILE_REQUEST:
                Snackbar.make(getErrorRoot(), R.string.error_while_request, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_UNAUTHORISED:
                Toast.makeText(getActivity().getApplicationContext(), R.string.error_unauthorised, Toast.LENGTH_LONG).show();
                AppPreferenceManager.getInstance(getActivity()).saveAuthCode("");
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
                startActivity(intent);
                break;
            case SERVICE_ERROR_BACKEND_CODE_ERROR:
                Snackbar.make(getErrorRoot(), R.string.error_backend, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_NGNIX_DOWN:
                Snackbar.make(getErrorRoot(), R.string.error_ngnix, Snackbar.LENGTH_LONG).show();
                break;
            case SERVICE_ERROR_IN_RECEIVED_DATA:
                Snackbar.make(getErrorRoot(), R.string.error_in_data, Snackbar.LENGTH_LONG).show();
                break;
        }


        hideLoad();
    }


    @Override
    public void onResume() {
        super.onResume();
        getLoaderManager().initLoader(1, null, this).forceLoad();
    }

    @Override
    public String getBroadcastFilter() {
        return this.getClass().getName();
    }


    @Override
    public Loader<ArrayList<Object>> onCreateLoader(int id, Bundle args) {
        showLoad();
        if(args!=null) {
            return new LombardListLoader(getActivity(), args.getStringArrayList(BUNDLE_ARR));
        } else {
            return new LombardListLoader(getActivity(), null);
        }
    }

    @Override
    public void onLoadFinished(Loader<ArrayList<Object>> loader, ArrayList<Object> data) {
        if (data.size() > 0) {
            mAdapter.addAll(data);
        }
        if (System.currentTimeMillis() - AppPreferenceManager.getInstance(getActivity()).getLombardUpdateDate() > 12000) {
            Intent intent = getIntent(SERVICE_GET_LOMBARD);
            getActivity().startService(intent);
        } else {
            hideLoad();
        }

    }

    @Override
    public void onLoaderReset(Loader<ArrayList<Object>> loader) {

    }

    @Override
    public void performClick(ArrayList<String> cities) {
        getLoaderManager().destroyLoader(1);
        Bundle b = new Bundle();
        b.putStringArrayList(BUNDLE_ARR, cities);
        getLoaderManager().initLoader(1, b, this).forceLoad();
    }

    @Override
    public void itemRemoved() {

    }
}

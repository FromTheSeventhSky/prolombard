package com.fromtheseventhsky.lombards.fragments.abstracts;

import android.content.Intent;
import android.support.v4.app.Fragment;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.activities.LoginActivity;
import com.fromtheseventhsky.lombards.activities.MainActivity;
import com.fromtheseventhsky.lombards.constants.IntConst;
import com.fromtheseventhsky.lombards.constants.StringConst;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public abstract class ChangeSelectionFragment extends Fragment implements StringConst, IntConst {
    protected void changeSelection(int fragmentNum){
        ((MainActivity)getActivity()).setSelection(fragmentNum);
    }

    protected void switchToMain(){
        Intent intent = new Intent(getActivity(), MainActivity.class);
        startActivity(intent);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
    }

    protected void switchToLogin(){
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.activity_fade_in, R.anim.activity_fade_out);
    }

}

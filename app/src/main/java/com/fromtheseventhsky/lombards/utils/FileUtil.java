package com.fromtheseventhsky.lombards.utils;

import android.graphics.Bitmap;
import android.os.Environment;
import android.widget.Toast;

import com.fromtheseventhsky.lombards.LombardApp;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class FileUtil {
    private void saveToInternalStorage(final Bitmap bitmapImage, String name) {
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            final File root = getFilePath(name);
            if(!root.exists()){
                if(!root.mkdirs()){
                    return;
                }
            }
            try {
                bitmapImage.compress(Bitmap.CompressFormat.JPEG, 80, new FileOutputStream(root));
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }
    public static boolean isPhotoExists(String name){
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            final File root = getFilePath(name);
            return root.exists();
        } else {
            return false;
        }
    }
    public static void createRoot(){
        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            final File root = getFilePath("");
            Toast.makeText(LombardApp.sAppContext, root.getAbsolutePath(), Toast.LENGTH_LONG).show();
            if(!root.exists()){
                if(!root.mkdirs()){
                    return;
                }
            }
        }
    }

    public static File getFilePath(String name){
        return new File(Environment.getExternalStorageDirectory().getAbsolutePath()
                .concat(File.separator)
                .concat("ProLombard")
                .concat(File.separator)
                .concat("Errors")
                .concat(File.separator)
                .concat(name));
    }
}

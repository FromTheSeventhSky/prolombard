package com.fromtheseventhsky.lombards.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.fromtheseventhsky.lombards.LombardApp;
import com.fromtheseventhsky.lombards.R;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class Utils {
    public static int getColor(Context context, @ColorRes int color){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            return context.getResources().getColor(color, context.getTheme());
        } else {
            return context.getResources().getColor(color);
        }
    }

    public static int convertToPixels(Context context, int nDP){
        final float conversionScale = context.getResources().getDisplayMetrics().density;

        return (int) ((nDP * conversionScale) + 0.5f) ;

    }

    public static Spannable parseWorkTime(String time){
        String[] arr = time.split(",");
        String beginTime = arr[0].trim();
        String endDate = arr[1].trim();
        String days = arr[2].trim();
        String resultDate = String.format(LombardApp.sAppContext.getString(R.string.lombard_work_time), beginTime, endDate);
        String workDays = LombardApp.sAppContext.getString(R.string.lombard_work_time_days);
        SpannableStringBuilder ssb = new SpannableStringBuilder(resultDate.concat(" ").concat(workDays));
        ssb.setSpan(new ForegroundColorSpan(Color.WHITE), 0, resultDate.length(), Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        int startLength = resultDate.length();
        for (int i = 0; i < 7; i++) {
            if(days.charAt(i)=='0'){
                ssb.setSpan(new ForegroundColorSpan(Color.RED), startLength, startLength+3, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            } else {
                ssb.setSpan(new ForegroundColorSpan(Color.GRAY), startLength, startLength+3, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
            }
            startLength+=3;
        }
        return ssb;
    }

    public static final String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isMyServiceRunning(Class<?> serviceClass, Context context) {
        ActivityManager manager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        int count = 0;
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                count++;
            }
        }
        return false;
    }

}

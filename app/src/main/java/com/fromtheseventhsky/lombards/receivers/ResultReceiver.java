package com.fromtheseventhsky.lombards.receivers;

import android.os.Bundle;
import android.os.Handler;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class ResultReceiver extends android.os.ResultReceiver {

    public interface Receiver {
        void onReceiveResult(int resultCode, Bundle resultData);
        void actionsWithResult(int resultCode, Bundle resultData);
    }

    private Receiver mReceiver;

    public ResultReceiver(Handler handler) {
        super(handler);
    }

    public void setReceiver(Receiver receiver) {
        mReceiver = receiver;
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if (mReceiver != null) {
            mReceiver.onReceiveResult(resultCode, resultData);
        }
    }

}
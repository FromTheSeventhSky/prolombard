package com.fromtheseventhsky.lombards.models;


import android.content.ContentValues;
import android.database.Cursor;

import com.fromtheseventhsky.lombards.constants.TableColumns;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LombardItem implements TableColumns {

    private String mLombardId;
    private String mLombardName;
    private String mLombardDescription;

    private String mLicenseNumber;
    private String mTelephones;
    private String mLombardAddress;
    private String mWorkTime;
    private String mLombardCoordinates;

    private String mSite;
    private String mEmail;

    private boolean mFavourite;

    private ArrayList<GoldItem> mGoldRates;
    private ArrayList<LoanRates> mLoanRates;

    private String mDocuments;
    private String mUpdateDate;

    private String mRegion;
    private String mIco;
    private String mImages;//доработать


    public LombardItem(Cursor cursor) {
        mLombardId = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_ID));
        mLombardName = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_TITLE));
        mLombardDescription = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_DESK));
        mLicenseNumber = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_LICENSE));
        mTelephones = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_PHONES));
        mLombardAddress = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_ADDRESS));
        mWorkTime = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_WORK_TIME));
        mLombardCoordinates = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_LAT_LON));
        mSite = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_SITE));
        mEmail = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_EMAIL));
        mUpdateDate = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_CREATE_DATE));
        mDocuments = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_DOC_LIST));
        mImages = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_IMAGES));
        mIco = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_ICO));
        mRegion = cursor.getString(cursor.getColumnIndex(COLUMN_LOMBARD_REGION));
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_LOMBARD_ID, mLombardId);
        cv.put(COLUMN_LOMBARD_TITLE, mLombardName);
        cv.put(COLUMN_LOMBARD_DESK, mLombardDescription);
        cv.put(COLUMN_LOMBARD_LICENSE, mLicenseNumber);
        cv.put(COLUMN_LOMBARD_PHONES, mTelephones);
        cv.put(COLUMN_LOMBARD_ADDRESS, mLombardAddress);
        cv.put(COLUMN_LOMBARD_WORK_TIME, mWorkTime);
        cv.put(COLUMN_LOMBARD_LAT_LON, mLombardCoordinates);
        cv.put(COLUMN_LOMBARD_SITE, mSite);
        cv.put(COLUMN_LOMBARD_EMAIL, mEmail);
        cv.put(COLUMN_LOMBARD_CREATE_DATE, mUpdateDate);
        cv.put(COLUMN_LOMBARD_DOC_LIST, mDocuments);
        cv.put(COLUMN_LOMBARD_IMAGES, mImages);
        cv.put(COLUMN_LOMBARD_ICO, mIco);
        cv.put(COLUMN_LOMBARD_REGION, mRegion);
        return cv;
    }

    public LombardItem() {

    }

    public String getLombardName() {
        return mLombardName;
    }

    public void setLombardName(String lombardName) {
        this.mLombardName = lombardName;
    }

    public String getLombardAddress() {
        return mLombardAddress;
    }

    public void setLombardAddress(String lombardAddress) {
        this.mLombardAddress = lombardAddress;
    }

    public String getLicenseNumber() {
        return mLicenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.mLicenseNumber = licenseNumber;
    }

    public String getWorkTime() {
        return mWorkTime;
    }

    public void setWorkTime(String workTime) {
        this.mWorkTime = workTime;
    }

    public String getLombardCoordinates() {
        return mLombardCoordinates;
    }

    public void setLombardCoordinates(String lombardCoordinates) {
        this.mLombardCoordinates = lombardCoordinates;
    }

    public String getLombardDescription() {
        return mLombardDescription;
    }

    public void setLombardDescription(String lombardDescription) {
        this.mLombardDescription = lombardDescription;
    }

    public String getTelephones() {
        return mTelephones;
    }

    public void setTelephones(String telephones) {
        this.mTelephones = telephones;
    }

    public String getSite() {
        return mSite;
    }

    public void setSite(String site) {
        this.mSite = site;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        this.mEmail = email;
    }

    public boolean isFavourite() {
        return mFavourite;
    }

    public void setFavourite(boolean favourite) {
        mFavourite = favourite;
    }

    public String getLombardId() {
        return mLombardId;
    }

    public void setLombardId(String lombardId) {
        this.mLombardId = lombardId;
    }

    public ArrayList<GoldItem> getGoldRates() {
        return mGoldRates;
    }

    public void setGoldRates(ArrayList<GoldItem> mGoldRates) {
        this.mGoldRates = mGoldRates;
    }

    public ArrayList<LoanRates> getLoanRates() {
        return mLoanRates;
    }

    public void setLoanRates(ArrayList<LoanRates> mLoanRates) {
        this.mLoanRates = mLoanRates;
    }

    public String getUpdateDate() {
        return mUpdateDate;
    }

    public void setUpdateDate(String mUpdateDate) {
        this.mUpdateDate = mUpdateDate;
    }

    public String getDocuments() {
        return mDocuments;
    }

    public void setDocuments(String mDocuments) {
        this.mDocuments = mDocuments;
    }

    public String getRegion() {
        return mRegion;
    }

    public void setRegion(String mRegion) {
        this.mRegion = mRegion;
    }

    public String getIco() {
        return mIco;
    }

    public void setIco(String mIco) {
        this.mIco = mIco;
    }

    public String getImages() {
        return mImages;
    }

    public void setImages(String mImages) {
        this.mImages = mImages;
    }
}

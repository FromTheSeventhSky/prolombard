package com.fromtheseventhsky.lombards.models;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class DrawerItem {

    private String mItemTitle;
    private int mIcon;

    public String getItemTitle() {
        return mItemTitle;
    }

    public void setItemTitle(String itemTitle) {
        this.mItemTitle = itemTitle;
    }

    public int getIcon() {
        return mIcon;
    }

    public void setIcon(int icon) {
        this.mIcon = icon;
    }

}

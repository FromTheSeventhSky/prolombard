package com.fromtheseventhsky.lombards.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.fromtheseventhsky.lombards.constants.TableColumns;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class GoldItem implements TableColumns {
    private String mId, mCarat, mPrice, mLombardId;

    public GoldItem() {
    }

    public GoldItem(Cursor cursor) {
        mId = cursor.getString(cursor.getColumnIndex(COLUMN_GOLD_ID));
        mCarat = cursor.getString(cursor.getColumnIndex(COLUMN_GOLD_CARAT));
        mPrice = cursor.getString(cursor.getColumnIndex(COLUMN_GOLD_PRICE));
        mLombardId = cursor.getString(cursor.getColumnIndex(COLUMN_GOLD_LOMBARD_ID));
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_GOLD_ID, mId);
        cv.put(COLUMN_GOLD_CARAT, mCarat);
        cv.put(COLUMN_GOLD_PRICE, mPrice);
        cv.put(COLUMN_GOLD_LOMBARD_ID, mLombardId);
        return cv;
    }


    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getCarat() {
        return mCarat;
    }

    public void setCarat(String mCarat) {
        this.mCarat = mCarat;
    }

    public String getPrice() {
        return mPrice;
    }

    public void setPrice(String mPrice) {
        this.mPrice = mPrice;
    }

    public String getLombardId() {
        return mLombardId;
    }

    public void setLombardId(String mLombardId) {
        this.mLombardId = mLombardId;
    }
}

package com.fromtheseventhsky.lombards.models;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class PreferenceItem {
    private boolean mIsCategory;
    private String mName;
    private String mDescription;
    private boolean mIsCheckable;
    private boolean mIsChecked;
    private int mPreferenceNumber;

    public boolean isCategory() {
        return mIsCategory;
    }

    public void setIsCategory(boolean mIsCategory) {
        this.mIsCategory = mIsCategory;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getDescription() {
        return mDescription;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public boolean isCheckable() {
        return mIsCheckable;
    }

    public void setCheckable(boolean mIsCheckable) {
        this.mIsCheckable = mIsCheckable;
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean mIsChecked) {
        this.mIsChecked = mIsChecked;
    }

    public int getPreferenceNumber() {
        return mPreferenceNumber;
    }

    public void setPreferenceNumber(int mPreferenceNumber) {
        this.mPreferenceNumber = mPreferenceNumber;
    }
}

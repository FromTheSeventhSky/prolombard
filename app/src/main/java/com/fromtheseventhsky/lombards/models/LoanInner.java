package com.fromtheseventhsky.lombards.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.fromtheseventhsky.lombards.constants.TableColumns;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LoanInner implements TableColumns {
    private String mId, mName, mRate, mParentId;
    private int mColor;

    public LoanInner() {
        mColor = -1;
    }

    public LoanInner(Cursor cursor) {
        mId = cursor.getString(cursor.getColumnIndex(COLUMN_LOAN_INNER_ID));
        mName = cursor.getString(cursor.getColumnIndex(COLUMN_LOAN_INNER_NAME));
        mRate = cursor.getString(cursor.getColumnIndex(COLUMN_LOAN_INNER_RATE));
        mParentId = cursor.getString(cursor.getColumnIndex(COLUMN_LOAN_INNER_PARENT_ID));
        mColor = -1;
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_LOAN_INNER_ID, mId);
        cv.put(COLUMN_LOAN_INNER_NAME, mName);
        cv.put(COLUMN_LOAN_INNER_RATE, mRate);
        cv.put(COLUMN_LOAN_INNER_PARENT_ID, mParentId);
        return cv;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getRate() {
        return mRate;
    }

    public void setRate(String mRate) {
        this.mRate = mRate;
    }

    public String getParentId() {
        return mParentId;
    }

    public void setParentId(String mParentId) {
        this.mParentId = mParentId;
    }

    public int getColor() {
        return mColor;
    }

    public void setColor(int mColor) {
        this.mColor = mColor;
    }
}

package com.fromtheseventhsky.lombards.models;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class AdItem {
    private String mSmallUrl, mLargeItem, mUrl;

    public String getSmallUrl() {
        return mSmallUrl;
    }

    public void setSmallUrl(String mSmallUrl) {
        this.mSmallUrl = mSmallUrl;
    }

    public String getLargeItem() {
        return mLargeItem;
    }

    public void setLargeItem(String mLargeItem) {
        this.mLargeItem = mLargeItem;
    }

    public String getUrl() {
        return mUrl;
    }

    public void setUrl(String mUrl) {
        this.mUrl = mUrl;
    }
}

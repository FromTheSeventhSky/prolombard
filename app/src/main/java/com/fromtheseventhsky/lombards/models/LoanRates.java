package com.fromtheseventhsky.lombards.models;

import android.content.ContentValues;
import android.database.Cursor;

import com.fromtheseventhsky.lombards.constants.TableColumns;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LoanRates implements TableColumns {
    private String mId, mName, mLombardId;
    private ArrayList<LoanInner> mLoans;

    public LoanRates() {
    }

    public LoanRates(Cursor cursor) {
        mId = cursor.getString(cursor.getColumnIndex(COLUMN_LOAN_ID));
        mName = cursor.getString(cursor.getColumnIndex(COLUMN_LOAN_NAME));
        mLombardId = cursor.getString(cursor.getColumnIndex(COLUMN_LOAN_LOMBARD_ID));
    }

    public ContentValues getContentValues() {
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_LOAN_ID, mId);
        cv.put(COLUMN_LOAN_NAME, mName);
        cv.put(COLUMN_LOAN_LOMBARD_ID, mLombardId);
        return cv;
    }


    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public ArrayList<LoanInner> getLoans() {
        return mLoans;
    }

    public void setLoans(ArrayList<LoanInner> mLoans) {
        this.mLoans = mLoans;
    }

    public String getLombardId() {
        return mLombardId;
    }

    public void setLombardId(String mLombardId) {
        this.mLombardId = mLombardId;
    }
}

package com.fromtheseventhsky.lombards.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class RegisterError implements Parcelable {
    private String mKey, mValue;

    public String getValue() {
        return mValue;
    }

    public void setValue(String mValue) {
        this.mValue = mValue;
    }

    public String getKey() {
        return mKey;
    }

    public void setKey(String mKey) {
        this.mKey = mKey;
    }

    public RegisterError(Parcel in) {
        readFromParcel(in);
    }
    public RegisterError() {
    }

    private void readFromParcel(Parcel in) {
        this.mKey = in.readString();
        this.mValue = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public final Parcelable.Creator<RegisterError> CREATOR = new Parcelable.Creator<RegisterError>() {
        public RegisterError createFromParcel(Parcel in) {
            return new RegisterError(in);
        }

        public RegisterError[] newArray(int size) {
            return new RegisterError[size];
        }
    };


    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mKey);
        dest.writeString(mValue);
    }

}

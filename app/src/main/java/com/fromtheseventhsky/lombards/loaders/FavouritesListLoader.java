package com.fromtheseventhsky.lombards.loaders;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.content.AsyncTaskLoader;

import com.fromtheseventhsky.lombards.constants.TableColumns;
import com.fromtheseventhsky.lombards.managers.DatabaseManager;
import com.fromtheseventhsky.lombards.managers.NetworkManager;
import com.fromtheseventhsky.lombards.models.GoldItem;
import com.fromtheseventhsky.lombards.models.LoanInner;
import com.fromtheseventhsky.lombards.models.LoanRates;
import com.fromtheseventhsky.lombards.models.LombardItem;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class FavouritesListLoader extends AsyncTaskLoader<ArrayList<Object>> implements TableColumns {


    private DatabaseManager mDatabaseManager;
    private NetworkManager mNetworkManager;
    private Context mContext;
    private ArrayList<String> mCities;

    public FavouritesListLoader(Context context, ArrayList<String> cities) {
        super(context);
        mContext = context;
        mCities = cities;
        mDatabaseManager = DatabaseManager.getInstance(context);
        mNetworkManager = NetworkManager.getInstance(context);
    }

    @Override
    public ArrayList<Object> loadInBackground() {

        ArrayList<Object> objects = new ArrayList<>();
        ArrayList<LombardItem> lombardItems = new ArrayList<>();

        String table = TABLE_LOMBARD_LIST
                .concat(" as LL inner join ")
                .concat(TABLE_FAVOURITES_LIST)
                .concat(" as FL on LL.")
                .concat(COLUMN_LOMBARD_ID)
                .concat(" = FL.")
                .concat(COLUMN_FAVOURITES_LOMBARD_ID);
        Cursor lombardList;
        if(mCities==null) {
            lombardList = mDatabaseManager.getAllRows(table);
        } else {
            String where = COLUMN_LOMBARD_REGION.concat(" = ?");
            String[] whereArgs = mCities.toArray(new String[mCities.size()]);
            lombardList = mDatabaseManager.getAllRows(table, where, whereArgs);
        }
        if(lombardList.moveToFirst()) {
            do {
                LombardItem lombardItem = new LombardItem(lombardList);
                String where = COLUMN_LOAN_LOMBARD_ID.concat(" = ?");
                String[] whereArgs = { lombardItem.getLombardId() };
                Cursor loanList = mDatabaseManager.getAllRows(TABLE_LOAN_LIST, where, whereArgs);
                ArrayList<LoanRates> loanRates = new ArrayList<>();
                if(loanList.moveToFirst()) {
                    do {
                        LoanRates loanRate = new LoanRates(loanList);
                        String whereInner = COLUMN_LOAN_INNER_PARENT_ID.concat(" = ?");
                        String[] whereArgsInner = { loanRate.getId() };
                        Cursor loanInnerList = mDatabaseManager.getAllRows(TABLE_LOAN_INNER_LIST, whereInner, whereArgsInner);
                        ArrayList<LoanInner> loanInnerRates = new ArrayList<>();
                        if(loanInnerList.moveToFirst()) {
                            do {
                                loanInnerRates.add(new LoanInner(loanInnerList));
                            } while (loanInnerList.moveToNext());
                            loanInnerList.close();
                        }
                        loanRate.setLoans(loanInnerRates);
                        loanRates.add(loanRate);
                    } while (loanList.moveToNext());
                }
                lombardItem.setLoanRates(loanRates);


                String whereGold = COLUMN_GOLD_LOMBARD_ID.concat(" = ?");
                String[] whereArgsGold = { lombardItem.getLombardId() };
                Cursor loanListGold = mDatabaseManager.getAllRows(TABLE_GOLD, whereGold, whereArgsGold);
                ArrayList<GoldItem> goldItems = new ArrayList<>();
                if(loanListGold.moveToFirst()) {
                    do {
                        GoldItem goldItem = new GoldItem(loanListGold);
                        goldItems.add(goldItem);
                    } while (loanListGold.moveToNext());
                }
                lombardItem.setGoldRates(goldItems);


                String whereFaw = COLUMN_FAVOURITES_LOMBARD_ID.concat(" = ?");
                String[] whereArgsFaw = { lombardItem.getLombardId() };
                Cursor faw = mDatabaseManager.getAllRows(TABLE_FAVOURITES_LIST, whereFaw, whereArgsFaw);
                if(faw.moveToFirst()) {
                    lombardItem.setFavourite(true);
                } else {
                    lombardItem.setFavourite(false);
                }
                lombardItems.add(lombardItem);
                loanList.close();
            } while (lombardList.moveToNext());
        }
        lombardList.close();
        objects.addAll(lombardItems);
        return objects;
    }
}

package com.fromtheseventhsky.lombards.loaders;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;

import com.fromtheseventhsky.lombards.constants.TableColumns;
import com.fromtheseventhsky.lombards.managers.NetworkManager;
import com.fromtheseventhsky.lombards.managers.ParserManager;
import com.fromtheseventhsky.lombards.models.AdItem;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public abstract class BannerSuperclass extends AsyncTaskLoader<ArrayList<Object>> implements TableColumns {

    private NetworkManager mNetworkManager;
    protected Context mContext;

    public BannerSuperclass(Context context) {
        super(context);
        mContext = context;
        mNetworkManager = NetworkManager.getInstance(context);
    }
    public ArrayList<AdItem> getBanners() {
        ArrayList<AdItem> banners = null;
        if (mNetworkManager.isOnline()) {
            String url = "http://lombardapi.rustik.uz/api/banners";
            String json = mNetworkManager.getJsonForBanners(url);
            banners = ParserManager.parseAd(json);
        }
        return banners;
    }

}

package com.fromtheseventhsky.lombards;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import com.fromtheseventhsky.lombards.utils.ExceptionUtil;
import com.fromtheseventhsky.lombards.utils.FileUtil;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LombardApp extends Application implements Application.ActivityLifecycleCallbacks {
    //public static GoogleAnalytics analytics;
    //public static Tracker tracker;

    public static Context sAppContext;


    @Override
    public void onCreate() {
        super.onCreate();

        sAppContext = getApplicationContext();
        if(getString(R.string.errors_api).equalsIgnoreCase("debug")){
            FileUtil.createRoot();
            // TODO: 06.04.2016 error listener or analytic
            if(!(Thread.getDefaultUncaughtExceptionHandler() instanceof ExceptionUtil)) {
                Thread.setDefaultUncaughtExceptionHandler(new ExceptionUtil(
                        FileUtil.getFilePath("").getPath()));
            }
        }
        //analytics = GoogleAnalytics.getInstance(this);

        //tracker = analytics.newTracker("UA-39876392-3");
        //tracker.enableExceptionReporting(true);
        //tracker.enableAdvertisingIdCollection(true);
        //tracker.enableAutoActivityTracking(true);
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onActivityCreated(Activity activity, Bundle savedInstanceState) {

    }

    @Override
    public void onActivityStarted(Activity activity) {

    }

    @Override
    public void onActivityResumed(Activity activity) {

    }

    @Override
    public void onActivityPaused(Activity activity) {

    }

    @Override
    public void onActivityStopped(Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(Activity activity, Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(Activity activity) {

    }
}

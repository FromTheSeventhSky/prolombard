package com.fromtheseventhsky.lombards.fcm;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class LombardsFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Log.d("test", ""+remoteMessage.getData());
    }
}

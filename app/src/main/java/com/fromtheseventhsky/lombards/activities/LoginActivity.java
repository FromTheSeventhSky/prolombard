package com.fromtheseventhsky.lombards.activities;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.fragments.login.LoginFragment;
import com.fromtheseventhsky.lombards.utils.Utils;
import com.google.firebase.FirebaseApp;

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    public void setColorOverscroll() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            int glowDrawableId = getResources().getIdentifier("overscroll_glow", "drawable", "android");
            int edgeDrawableId = getResources().getIdentifier("overscroll_edge", "drawable", "android");
            if (glowDrawableId == 0 && edgeDrawableId == 0) {
                return;
            }
            Drawable androidGlow = this.getResources().getDrawable(glowDrawableId);
            if (androidGlow != null) {
                androidGlow.setColorFilter(Utils.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            }

            final Drawable overscrollEdge = getResources().getDrawable(edgeDrawableId);
            if (overscrollEdge != null) {
                overscrollEdge.setColorFilter(Utils.getColor(this, R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FirebaseApp.getInstance();
        Bundle notification = getIntent().getExtras();

        setContentView(R.layout.activity_login);
        setColorOverscroll();
        
        findViewById(R.id.scroll).setEnabled(false);
        findViewById(R.id.background_img).setOnClickListener(this);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getSupportFragmentManager().beginTransaction().replace(R.id.register_lay, LoginFragment.newInstance()).commit();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.background_img:
                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).
                        hideSoftInputFromWindow(v.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                break;
        }
    }
}

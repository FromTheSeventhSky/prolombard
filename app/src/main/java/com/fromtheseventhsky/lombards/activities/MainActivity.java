package com.fromtheseventhsky.lombards.activities;

import android.animation.Animator;
import android.animation.ValueAnimator;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.fromtheseventhsky.lombards.R;
import com.fromtheseventhsky.lombards.adapters.DrawerAdapter;
import com.fromtheseventhsky.lombards.constants.IntConst;
import com.fromtheseventhsky.lombards.fragments.AboutProjectFragment;
import com.fromtheseventhsky.lombards.fragments.AccountReturnAmountFragment;
import com.fromtheseventhsky.lombards.fragments.HelpFragment;
import com.fromtheseventhsky.lombards.fragments.MapFragment;
import com.fromtheseventhsky.lombards.fragments.PreferencesFragment;
import com.fromtheseventhsky.lombards.fragments.login.UnLoginFragment;
import com.fromtheseventhsky.lombards.fragments.lombards.FavouritesFragment;
import com.fromtheseventhsky.lombards.fragments.lombards.LombardListFragment;
import com.fromtheseventhsky.lombards.fragments.lombards.NotificationsFragment;
import com.fromtheseventhsky.lombards.managers.AppPreferenceManager;
import com.fromtheseventhsky.lombards.models.DrawerItem;
import com.fromtheseventhsky.lombards.utils.Utils;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, IntConst {

    protected DrawerLayout mDrawerLayout;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected Toolbar mToolbar;
    protected CharSequence mTitle;
    private LinearLayout mDrawerRoot;
    protected ListView mDrawerList;
    private DrawerAdapter mDrawerAdapter;

    private FrameLayout mRoot;

    private View.OnClickListener mOriginalToolbarListener;
    private View.OnClickListener mBackNavigation;

    private int mInnerLevel;

    private ValueAnimator mArrowAnim;

    private boolean mDrawerLocked;


    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        mToolbar.setTitle(mTitle);
    }

    public void toggleColorToolbar(boolean def) {
        if (mToolbar != null) {
            if (def) {
                mToolbar.setBackgroundColor(Utils.getColor(this, R.color.colorPrimary));
            } else {
                mToolbar.setBackgroundColor(Utils.getColor(this, R.color.black));
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(mDrawerRoot)) {
            mDrawerLayout.closeDrawer(mDrawerRoot);
            return;
        }
        if (mInnerLevel == 1) {
            if (mDrawerLocked) {
                unlockDrawer();
                mInnerLevel--;
            }
        } else {
            if (mInnerLevel > 0) {
                mInnerLevel--;
            }
        }
        super.onBackPressed();
    }

    public void increaseInnerLevel() {
        mInnerLevel++;
    }

    public void setColorOverscroll() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            int glowDrawableId = getResources().getIdentifier("overscroll_glow", "drawable", "android");
            int edgeDrawableId = getResources().getIdentifier("overscroll_edge", "drawable", "android");
            if (glowDrawableId == 0 && edgeDrawableId == 0) {
                return;
            }
            Drawable androidGlow = this.getResources().getDrawable(glowDrawableId);
            if (androidGlow != null) {
                androidGlow.setColorFilter(Utils.getColor(this, R.color.colorPrimary), PorterDuff.Mode.SRC_IN);
            }

            final Drawable overscrollEdge = getResources().getDrawable(edgeDrawableId);
            if (overscrollEdge != null) {
                overscrollEdge.setColorFilter(Utils.getColor(this, R.color.colorPrimary), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        }
    }

    public View getActivityContent() {
        return mRoot;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            Intent intent = new Intent(this, LoginActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_ANIMATION);
            startActivity(intent);
            return;
        }
        super.onCreate(savedInstanceState);



        FirebaseApp.getInstance();
        Log.d("token", ""+FirebaseInstanceId.getInstance().getToken());

        setContentView(R.layout.activity_main);
        AppPreferenceManager.getInstance(this).loadLocale();
        mRoot = (FrameLayout) findViewById(R.id.container);
        setColorOverscroll();
        mDrawerRoot = (LinearLayout) findViewById(R.id.left_drawer_root);

        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerAdapter = new DrawerAdapter(this, getDrawerItems());
        mDrawerList.setAdapter(mDrawerAdapter);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);

        if (mToolbar != null) {
            setSupportActionBar(mToolbar);
            mToolbar.setTitle(mTitle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (mDrawerLayout != null) {
            initDrawer();
        }

        mOriginalToolbarListener = mDrawerToggle.getToolbarNavigationClickListener();
        mBackNavigation = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().popBackStack();
                if (mInnerLevel == 1) {
                    if (mDrawerLocked) {
                        unlockDrawer();
                        mInnerLevel--;
                    }
                } else {
                    if (mInnerLevel > 0) {
                        mInnerLevel--;
                    }
                }
            }
        };

        clickOnDrawerItem(0);
    }

    public void lockDrawer() {
        mDrawerLocked = true;

        if (mArrowAnim != null && mArrowAnim.isRunning()) {
            mArrowAnim.cancel();
        }
        mArrowAnim = ValueAnimator.ofFloat(0, 1);
        mArrowAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset);
            }
        });
        mArrowAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                mDrawerToggle.setDrawerIndicatorEnabled(false);
                mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
                mDrawerToggle.setToolbarNavigationClickListener(mBackNavigation);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mArrowAnim.setInterpolator(new DecelerateInterpolator());
        mArrowAnim.setDuration(300);
        mArrowAnim.start();
    }


    public void unlockDrawer() {
        mDrawerLocked = false;

        if (mArrowAnim != null && mArrowAnim.isRunning()) {
            mArrowAnim.cancel();
        }
        mArrowAnim = ValueAnimator.ofFloat(1, 0);
        mArrowAnim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                float slideOffset = (Float) valueAnimator.getAnimatedValue();
                mDrawerToggle.onDrawerSlide(mDrawerLayout, slideOffset);
            }
        });
        mArrowAnim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
        mDrawerToggle.setDrawerIndicatorEnabled(true);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        mDrawerToggle.setToolbarNavigationClickListener(mOriginalToolbarListener);
        mArrowAnim.setInterpolator(new DecelerateInterpolator());
        mArrowAnim.setDuration(300);
        mArrowAnim.start();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mDrawerList != null) {
            mDrawerList.setOnItemClickListener(this);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mDrawerList != null) {
            mDrawerList.setOnItemClickListener(null);
        }
    }

    private void initDrawer() {
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar,
                R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                mToolbar.setTitle(mTitle);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mToolbar.setTitle(R.string.menu_drawer_title);
                invalidateOptionsMenu();
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.syncState();
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mDrawerToggle != null) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle != null) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        mDrawerLayout.closeDrawer(mDrawerRoot);
        clickOnDrawerItem(position);
    }

    public void clickOnDrawerItem(int position) {
        if (position == mDrawerAdapter.getSelectedPosition()) {
            return;
        }
        String fragmentName = "";
        switch (position) {
            case LOMBARD_LIST:
                fragmentName = LombardListFragment.class.getName();
                break;
            case CABINET:
                fragmentName = AccountReturnAmountFragment.class.getName();
                break;
            case LOMBARD_MAP:
                fragmentName = MapFragment.class.getName();
                break;
            case NOTIFICATIONS_LIST:
                fragmentName = NotificationsFragment.class.getName();
                break;
            case FAVORITE:
                fragmentName = FavouritesFragment.class.getName();
                break;
            case PREFERENCES:
                fragmentName = PreferencesFragment.class.getName();
                break;
            case ABOUT:
                fragmentName = AboutProjectFragment.class.getName();
                break;
            case HELP:
                fragmentName = HelpFragment.class.getName();
                break;
            case EXIT:
                fragmentName = UnLoginFragment.class.getName();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.add(R.id.container, Fragment.instantiate(this, fragmentName, null), "exit");
                fragmentTransaction.commit();
                return;
        }
        mDrawerAdapter.setSelectedPosition(position);
        if (!fragmentName.isEmpty()) {
            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.container);
            if (fragment != null) {
                fragmentTransaction.replace(R.id.container, Fragment.instantiate(this, fragmentName, null));
                fragmentTransaction.addToBackStack(null);
            } else {
                fragmentTransaction.replace(R.id.container, Fragment.instantiate(this, fragmentName, null));
            }
            fragmentTransaction.commit();
        }
    }

    public void setSelection(int position) {
        mDrawerAdapter.setSelectedPosition(position);
        mDrawerAdapter.notifyDataSetChanged();
    }


    private ArrayList<DrawerItem> getDrawerItems() {
        ArrayList<DrawerItem> items = new ArrayList<>();
        Resources resources = getResources();

        String[] titles = resources.getStringArray(R.array.drawer_titles);
        TypedArray typedArray = resources.obtainTypedArray(R.array.drawer_icons);
        for (int i = 0; i < titles.length; i++) {
            DrawerItem drawerItem = new DrawerItem();
            drawerItem.setItemTitle(titles[i]);
            drawerItem.setIcon(typedArray.getResourceId(i, 0));
            items.add(drawerItem);
        }
        typedArray.recycle();

        return items;
    }


}

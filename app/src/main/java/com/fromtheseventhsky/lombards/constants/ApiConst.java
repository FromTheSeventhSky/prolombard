package com.fromtheseventhsky.lombards.constants;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public interface ApiConst {
    String MAIN_URL = "http://lombardapi.rustik.uz/";

    String REGISTER_URL = MAIN_URL + "api/users";
    String SIGN_URL = MAIN_URL + "api/sessions";


    String GET_LOMBARDS_WITHOUT_DATE = MAIN_URL + "api/lombards/?page";
    String GET_LOMBARDS_WITH_DATE = MAIN_URL + "api/lombards/?since=";

    String CLOUD_PROJECT_ID = "192995563687";
}

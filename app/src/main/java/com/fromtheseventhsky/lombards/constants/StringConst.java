package com.fromtheseventhsky.lombards.constants;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public interface StringConst {
    String RECEIVER = "result_receiver";

    String SERVICE_ACTION = "network_service_action";

    String SERVICE_STR_ARR_BUNDLE = "SERVICE_STR_ARR_BUNDLE";

    String HANDLER_IMG_NUM = "handler_img";
    String HANDLER_RESULT_CODE = "handler_result";

    String NETWORK_SERVICE_ACTION = "NETWORK_SERVICE_ACTION";
    String SERVICE_RESULT = "SERVICE_RESULT";
    String SERVICE_ERROR_RESULT = "SERVICE_ERROR_RESULT";
    String SERVICE_ERROR_RESULT_MSG = "SERVICE_ERROR_RESULT_MSG";

    String BUNDLE_ARR = "b_arr";


    String ARGUMENT_IMAGE = "arg_image";
    String ARGUMENT_URL = "arg_url";

}

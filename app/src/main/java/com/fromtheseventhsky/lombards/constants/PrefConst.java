package com.fromtheseventhsky.lombards.constants;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public interface PrefConst {
    String PREFERENCE_FILE_NAME = "prolombard";
    String PREFERENCE_LANGUAGE = "pref_language";
    String PREFERENCE_AGREEMENT = "pref_agreement";

    String PREFERENCE_AUTH = "pref_auth";
    String PREFERENCE_UPDATE_LOMBARD = "pref_upd_lom";
    String PREFERENCE_NOTIFICATIONS_STATE = "pref_not_stat";
    String PREFERENCE_NOTIFICATIONS_SOUND = "pref_not_sou";
}

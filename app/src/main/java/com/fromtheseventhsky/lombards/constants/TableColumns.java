package com.fromtheseventhsky.lombards.constants;

import android.provider.BaseColumns;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public interface TableColumns {

    String COLUMN_BASE_ID = BaseColumns._ID;

    String TABLE_LOMBARD_LIST = "lombards";

    String COLUMN_LOMBARD_ID = "id";
    String COLUMN_LOMBARD_TITLE = "title";
    String COLUMN_LOMBARD_ADDRESS = "address";
    String COLUMN_LOMBARD_LICENSE = "license";
    String COLUMN_LOMBARD_WORK_TIME = "work_time";
    String COLUMN_LOMBARD_DESK = "desc";
    String COLUMN_LOMBARD_PHONES = "phones";
    String COLUMN_LOMBARD_SITE = "site";
    String COLUMN_LOMBARD_EMAIL = "email";
    String COLUMN_LOMBARD_PHOTOS = "photos";
    String COLUMN_LOMBARD_LAT_LON = "latlon";
    String COLUMN_LOMBARD_DOC_LIST = "docs";

    String COLUMN_LOMBARD_IMAGES = "lombard_images";
    String COLUMN_LOMBARD_ICO = "logo_url";
    String COLUMN_LOMBARD_REGION = "region_name";

    String COLUMN_LOMBARD_CREATE_DATE = "update_date";


    String TABLE_FAVOURITES_LIST = "favourites";
    String COLUMN_FAVOURITES_LOMBARD_ID = "id";


    String TABLE_LOAN_LIST = "loan";

    String COLUMN_LOAN_ID = "id";
    String COLUMN_LOAN_LOMBARD_ID = "lombardid";
    String COLUMN_LOAN_NAME = "name";

    String TABLE_LOAN_INNER_LIST = "loan_inner";

    String COLUMN_LOAN_INNER_ID = "id";
    String COLUMN_LOAN_INNER_PARENT_ID = "parentid";
    String COLUMN_LOAN_INNER_NAME = "name";
    String COLUMN_LOAN_INNER_RATE = "rate";

    String TABLE_GOLD = "gold";

    String COLUMN_GOLD_ID = "id";
    String COLUMN_GOLD_LOMBARD_ID = "lombardid";
    String COLUMN_GOLD_CARAT = "carat";
    String COLUMN_GOLD_PRICE = "price";


    String TABLE_FILTER = "filter";

    String COLUMN_FILTER_ID = "id";
    String COLUMN_FILTER_LOMBARD_ID = "lombardid";

}

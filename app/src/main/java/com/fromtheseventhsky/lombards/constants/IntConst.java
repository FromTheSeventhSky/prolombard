package com.fromtheseventhsky.lombards.constants;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public interface IntConst {

    /*int LOMBARD_LIST = 0;
    int CABINET = 1;
    int LOMBARD_MAP = 2;
    int NOTIFICATIONS_LIST = 3;
    int FAVORITE = 4;
    int PREFERENCES = 5;
    int ABOUT = 6;
    int HELP = 7;
    int EXIT = 8;*/


    int LOMBARD_LIST = 0;
    int CABINET = 1;
    int LOMBARD_MAP = 2;
    int NOTIFICATIONS_LIST = 32;
    int FAVORITE = 3;
    int PREFERENCES = 52;
    int ABOUT = 62;
    int HELP = 72;
    int EXIT = 4;

    int BANNER = 20;
    int LIST_ITEM = 21;


    int LANGUAGE_TITLE = 0;
    int LANGUAGE = 1;
    int VOTE = 2;
    int VOTE_TITLE = 3;
    int AUTO_UPDATE = 4;
    int UPDATE_ONLY_WIFI = 5;
    int UPDATE_TITLE = 6;


    int HANDLER_LOADED = 556000;
    int HANDLER_INTERNET_ERROR = 556001;


    int SERVICE_ERROR = 85002;
    int SERVICE_RUNNING = 85003;
    int SERVICE_FINISH = 85004;

    int SERVICE_ERROR_NO_INTERNET = 850001;
    int SERVICE_ERROR_WHILE_REQUEST = 850002;
    int SERVICE_ERROR_IN_RECEIVED_DATA = 422;
    int SERVICE_ERROR_UNAUTHORISED = 401;
    int SERVICE_ERROR_BACKEND_CODE_ERROR = 500;
    int SERVICE_ERROR_NGNIX_DOWN = 503;

    int SERVICE_REGISTRATION = 459001;
    int SERVICE_LOGIN = 459002;
    int SERVICE_UNLOGIN = 459003;

    int SERVICE_GET_LOMBARD = 459005;


    int PERMISSION_CALL_CODE = 1;



    int PREFERENCE_ACCOUNT = 1;
    int PREFERENCE_NOTIFICATION = 2;
    int PREFERENCE_NOTIFICATION_SOUND = 3;
    int PREFERENCE_RATE = 4;
    int PREFERENCE_ABOUT_US = 5;
    int PREFERENCE_ABOUT_APP = 6;


}

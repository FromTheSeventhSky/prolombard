package com.fromtheseventhsky.lombards.managers;

import com.fromtheseventhsky.lombards.constants.ApiConst;
import com.fromtheseventhsky.lombards.models.AdItem;
import com.fromtheseventhsky.lombards.models.GoldItem;
import com.fromtheseventhsky.lombards.models.LoanInner;
import com.fromtheseventhsky.lombards.models.LoanRates;
import com.fromtheseventhsky.lombards.models.LombardItem;
import com.fromtheseventhsky.lombards.models.RegisterError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class ParserManager {


    public static ArrayList<RegisterError> getErrorsRegister(String jsonString) {
        if (jsonString == null || jsonString.equals("")) return null;

        ArrayList<RegisterError> errors = new ArrayList<>();
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            JSONObject jsonErr = jsonObject.getJSONObject("errors");
            JSONArray names = jsonErr.names();
            for (int i = 0; i < jsonErr.length(); i++) {
                JSONArray jsonArray = jsonErr.getJSONArray(names.getString(i));
                RegisterError re = new RegisterError();
                re.setKey(names.getString(i));
                re.setValue(jsonArray.getString(0));
                errors.add(re);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
        return errors;
    }

    public static String getErrorLogin(String jsonString) {
        if (jsonString == null || jsonString.equals("")) return null;
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            return jsonObject.getString("errors");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String parseAuthCode(String jsonString) {
        if (jsonString == null || jsonString.equals("")) return null;
        try {
            JSONObject jsonObject = new JSONObject(jsonString);
            return jsonObject.getString("auth_token");
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<AdItem> parseAd(String jsonString) {
        if (jsonString == null || jsonString.equals("")) return null;
        ArrayList<AdItem> items = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(jsonString);

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject obj = jsonArray.getJSONObject(i);
                AdItem ai = new AdItem();
                ai.setSmallUrl(ApiConst.MAIN_URL+obj.getString("image_small_url"));
                ai.setLargeItem(ApiConst.MAIN_URL+obj.getString("image_large_url"));
                ai.setUrl(obj.getString("url"));
                items.add(ai);
            }

            return items;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static ArrayList<LombardItem> parseLombards(String jsonString) {
        if (jsonString == null || jsonString.equals("")) return null;
        ArrayList<LombardItem> lombardItems = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(jsonString);
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                LombardItem li = new LombardItem();
                li.setLombardId(jsonObject.getString("id"));
                li.setLombardName(jsonObject.getString("name"));
                li.setLombardDescription(jsonObject.getString("description"));
                li.setLicenseNumber(jsonObject.getString("license"));
                JSONArray phones = jsonObject.getJSONArray("phones");
                String phonesList = "";
                for (int j = 0; j < phones.length(); j++) {
                    phonesList = phonesList.concat(phones.getString(j)).concat(",");
                }
                li.setTelephones(phonesList.trim());
                li.setLombardAddress(jsonObject.getString("address"));
                li.setSite(jsonObject.getString("website"));
                li.setEmail(jsonObject.getString("email"));
                li.setLombardCoordinates(jsonObject.getString("geolocation"));
                li.setWorkTime(jsonObject.getString("working_hours"));

                li.setIco(jsonObject.getString("logo_url"));
                li.setRegion(jsonObject.getString("region_name"));
                JSONArray images = jsonObject.getJSONArray("lombard_images");
                String imagesList = "";
                for (int j = 0; j < images.length(); j++) {
                    imagesList = imagesList.concat(images.getString(j)).concat(" ");
                }
                li.setImages(imagesList);

                String documentList = "";
                JSONArray documents = jsonObject.getJSONArray("documents");
                for (int j = 0; j < documents.length(); j++) {
                    documentList = documentList.concat(documents.getString(j)).concat(",");
                }
                li.setDocuments(documentList.trim());
                JSONArray loanTypes = jsonObject.getJSONArray("loan_types");
                ArrayList<LoanRates> loanRates = new ArrayList<>();
                for (int j = 0; j < loanTypes.length(); j++) {
                    JSONObject loanObj = loanTypes.getJSONObject(j);
                    LoanRates lr = new LoanRates();
                    lr.setId(loanObj.getString("id"));
                    lr.setLombardId(li.getLombardId());
                    lr.setName(loanObj.getString("name"));
                    JSONArray loanInnerTypes = loanObj.getJSONArray("loan_sub_types");
                    ArrayList<LoanInner> loanInnerRates = new ArrayList<>();
                    for (int k = 0; k < loanInnerTypes.length(); k++) {
                        JSONObject loanInnerObj = loanInnerTypes.getJSONObject(k);
                        LoanInner loanInner = new LoanInner();
                        loanInner.setParentId(lr.getId());
                        loanInner.setId(loanInnerObj.getString("id"));
                        loanInner.setName(loanInnerObj.getString("name"));
                        loanInner.setRate(loanInnerObj.getString("rate"));
                        loanInnerRates.add(loanInner);
                    }
                    lr.setLoans(loanInnerRates);
                    loanRates.add(lr);
                }
                li.setLoanRates(loanRates);
                JSONArray goldTypes = jsonObject.getJSONArray("gold_rates");
                ArrayList<GoldItem> goldRates = new ArrayList<>();
                for (int j = 0; j < goldTypes.length(); j++) {
                    GoldItem gi = new GoldItem();
                    JSONObject goldObj = goldTypes.getJSONObject(j);
                    gi.setLombardId(li.getLombardId());
                    gi.setId(goldObj.getString("id"));
                    gi.setCarat(goldObj.getString("carat"));
                    gi.setPrice(goldObj.getString("price"));
                    goldRates.add(gi);
                }
                li.setGoldRates(goldRates);
                lombardItems.add(li);
            }
            return lombardItems;
        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}

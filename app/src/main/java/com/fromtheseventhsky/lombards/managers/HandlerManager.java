package com.fromtheseventhsky.lombards.managers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.fromtheseventhsky.lombards.LombardApp;
import com.fromtheseventhsky.lombards.constants.IntConst;
import com.fromtheseventhsky.lombards.constants.StringConst;
import com.fromtheseventhsky.lombards.utils.FileUtil;
import com.fromtheseventhsky.lombards.utils.Utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by FromTheSeventhSky in 2016.
 * <p/>
 * Класс, необходимый для асинхронной загрузки изображений ломбардов,
 * а затем сохранения их на карту памяти. Работа производится по принципу
 * -Создается хешмеп с тредами, с хендлерами от главной страницы ломбарда и
 * хендлером от увеличенного изображения.
 */
public class HandlerManager implements IntConst, StringConst{
    private static HandlerManager sInstance;

    private Handler mHandlerMainMap;
    private Handler mHandlerInnerMap;
    private HashMap<String, Thread> mThreadMap;

    public static HandlerManager getInstance() {
        if (sInstance == null) {
            sInstance = new HandlerManager();
        }
        return sInstance;
    }

    protected HandlerManager() {
        mHandlerMainMap = null;
        mHandlerInnerMap = null;
        mThreadMap = new HashMap<String, Thread>();
    }

    public boolean isInQuery(String key) {
        return mThreadMap.containsKey(key);
    }

    private boolean isHandlerRegistered() {
        return mHandlerMainMap != null;
    }

    private boolean isInnerHandlerRegistered() {
        return mHandlerInnerMap != null;
    }

    /**
     * Метод, добавляющий Handler в HandlerManager для в данный момент открытого TabAboutUs
     */
    public void registerHandler(Handler handler) {
        mHandlerMainMap = handler;
    }

    /**
     * Метод, удаляющий Handler из HandlerManager для в данный момент открытого TabAboutUs
     */
    public void unregisterHandler() {
        mHandlerMainMap = null;
    }

    /**
     * Метод, добавляющий Handler в HandlerManager для в данный момент открытого ImagePreviewFragment
     */
    public void registerInnerHandler(Handler handler) {
        mHandlerInnerMap = handler;
    }

    /**
     * Метод, удаляющий Handler из HandlerManager для в данный момент открытого ImagePreviewFragment
     */
    public void unregisterInnerHandler(String key) {
        mHandlerInnerMap = null;
    }

    /*
     * Метод, добавляющий картинки в очередь на загрузку.
     * Алгоритм следующий: проверяем в отдельном потоке все ссылки на наличие уже загруженных фотографий.
     * Если пути есть, то ссылки добавляем в 1 массив, если нет то в другой. После этого, посылем в хендлеры
     * информацию о уже загруженных картинках, чтоб через пикассо подгрузить их из кеша. Далее, обращаемся в
     * массив с незагруженными картинками и в цикле их подгружаем и кэшируем.
     *
     */
    public boolean addToQuery(final String key, final String[] links) {

        if (isInQuery(key)) {
            return true;
        } else {
            Thread thread;
            thread = new Thread(new Runnable() {
                public void run() {
                    ArrayList<String> downloaded = new ArrayList<>();
                    ArrayList<String> missing = new ArrayList<>();
                    for (int i = 0; i < links.length; i++) {
                        String tmp = Utils.md5(links[i]+i+key);
                        if(FileUtil.isPhotoExists(tmp)){
                            downloaded.add(tmp);
                            missing.add("");
                        } else {
                            downloaded.add("");
                            missing.add(tmp);
                        }
                    }
                    for (int i = 0; i < downloaded.size(); i++) {
                        if(!downloaded.get(i).equals("")){
                            sendMessage(i, HANDLER_LOADED);
                        }
                    }
                    NetworkManager networkManager = NetworkManager.getInstance(LombardApp.sAppContext);
                    for (int i = 0; i < missing.size(); i++) {
                        if(!missing.get(i).equals("")){
                            //download and cache
                            if(networkManager.isOnline()){
                                InputStream is = networkManager.getInputStream("http://air.gio.uz//base/photo/city_25.jpg");
                                sendMessage(i, HANDLER_LOADED);
                                //try {
                                //    BitmapFactory.decodeStream(is)
                                //            .compress(Bitmap.CompressFormat.JPEG, 80, new FileOutputStream(FileUtil.getFilePath(missing.get(i))));
                                //} catch (FileNotFoundException e) {
                                //    e.printStackTrace();
                                //}
                            } else {
                                sendMessage(i, HANDLER_INTERNET_ERROR);
                            }
                        }
                    }


                }
            });
            mThreadMap.put(key, thread);
            thread.start();

            return false;
        }
    }

    private void sendMessage(int whichImg, int resultCode){
        Message msg = new Message();
        Bundle bundle = new Bundle();
        bundle.putInt(HANDLER_IMG_NUM, whichImg);
        bundle.putInt(HANDLER_RESULT_CODE, resultCode);
        msg.setData(bundle);
        if (isHandlerRegistered()) {
            mHandlerMainMap.sendMessage(msg);
        }
        if (isInnerHandlerRegistered()) {
            mHandlerInnerMap.sendMessage(msg);
        }
    }
}

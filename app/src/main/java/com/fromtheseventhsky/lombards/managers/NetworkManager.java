package com.fromtheseventhsky.lombards.managers;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.fromtheseventhsky.lombards.LombardApp;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class NetworkManager {

    private static NetworkManager sInstance;
    private final Context mContext;
    private OkHttpClient mHttpClient;
    private InputStream mInputStream;

    private NetworkManager(Context context) {
        mContext = context.getApplicationContext();
        mHttpClient = new OkHttpClient();

    }

    public static NetworkManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new NetworkManager(context);
        }
        return sInstance;
    }


    /**
     * Проверка на наличие соеденения
     *
     * @return boolean
     */
    public boolean isOnline() {
        ConnectivityManager manager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    /**
     * Возвращаем результат запроса по url
     *
     * @return JSON String
     */
    public String getJSONString(Response response) {
        try {
            return response.body().string();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public InputStream getInputStream(String url) {
        try {
            if (mInputStream != null) {
                mInputStream.close();
            }

            Response response = mHttpClient.newCall(getRequest(url)).execute();
            return mInputStream = response.body().byteStream();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public Response getResponseForRegister(String url, String[] args){
        try {
            return mHttpClient.newCall(getRequestForRegister(url, args)).execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public Response getResponseForSign(String url, String[] args){
        try {
            return mHttpClient.newCall(getRequestForAuthToken(url, args)).execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Response getResponseForUnSign(String url){
        try {
            return mHttpClient.newCall(getRequestForUnLogin(url)).execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Response getResponseForGetLombards(String url){
        try {
            return mHttpClient.newCall(getRequestForGetLombards(url)).execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getJsonForBanners(String url) {
        try {
            return getResponseForBanners(url).body().string();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public Response getResponseForBanners(String url){
        try {
            return mHttpClient.newCall(getRequestForBanners(url)).execute();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Request getRequestForBanners(String url) {
        String authCode = AppPreferenceManager.getInstance(LombardApp.sAppContext).getAuthCode();
        return new Request.Builder().url(url).get()
                .addHeader("Authorization", authCode).build();
    }

    /**
     * Строим Request
     *
     * @param url ссылка
     * @return Request
     */
    private Request getRequest(String url) {
        return new Request.Builder().url(url).get().build();
    }

    private Request getRequestForAuthToken(String url, String[] args) {
        RequestBody rb = new FormEncodingBuilder()
                .add("session[username]", args[0])
                .add("session[password]", args[1])
                .build();
        return new Request.Builder().url(url).post(rb).build();
    }

    private Request getRequestForUnLogin(String url) {
        return new Request.Builder().url(url).delete().build();
    }

    private Request getRequestForRegister(String url, String[] args) {
        RequestBody rb = new FormEncodingBuilder()
                .add("user[email]", args[0])
                .add("user[password]", args[1])
                .add("user[password_confirmation]", args[2])
                .add("user[username]", args[3])
                .add("user[full_name]", args[4])
                .add("user[phone]", args[5])
                .build();
        return new Request.Builder().url(url).post(rb).build();
    }

    private Request getRequestForGetUser(String url) {
        String authCode = AppPreferenceManager.getInstance(LombardApp.sAppContext).getAuthCode();
        return new Request.Builder().url(url).get()
                .addHeader("Authorization", authCode)
                .addHeader("Content-Type", "application/json").build();
    }

    private Request getRequestForGetLombards(String url) {
        String authCode = AppPreferenceManager.getInstance(LombardApp.sAppContext).getAuthCode();
        return new Request.Builder().url(url).get()
                .addHeader("Authorization", authCode)
                .addHeader("Content-Type", "application/json").build();
    }

}

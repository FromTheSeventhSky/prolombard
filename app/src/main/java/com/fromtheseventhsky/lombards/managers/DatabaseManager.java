package com.fromtheseventhsky.lombards.managers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.fromtheseventhsky.lombards.constants.TableColumns;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class DatabaseManager implements TableColumns {
    private static DatabaseManager sInstance;
    private DBHelper mDBHelper;
    private DatabaseManager(Context context) {
        mDBHelper = new DBHelper(context.getApplicationContext());
    }

    public static DatabaseManager getInstance(Context context) {
        if (sInstance == null) {
            sInstance = new DatabaseManager(context);
        }
        return sInstance;
    }

    //METHODS
    /**
     * Получаем все записи из таблицы
     * @param tableName имя таблицы
     * @param where условие, по которому будут запрошены записи
     * @param whereArgs массив аргументов
     * @param columnName имя столбца по которому сортировать
     * @param order сортировка по возрастанию если true, пример ( 1 3 4 5 8 32 98 )
     * @param distinct результаты не будут дублироваться
     * @return Cursor
     */
    public Cursor getAllRows(String tableName, String where, String[] whereArgs, String columnName, boolean order, boolean distinct) {
        String orderBy = null;

        if (columnName != null) {
            String orderClause = order ? "ASC":"DESC";
            orderBy = columnName + " " + orderClause;
        }
        return mDBHelper.getReadableDatabase().query(true, tableName, null, where, whereArgs, null, null, orderBy, null);
    }
    /**
     * Получаем все записи из таблицы
     * @param tableName имя таблицы
     * @param where условие, по которому будут запрошены записи
     * @param whereArgs массив аргументов
     * @param columnNames группа столбцов по которым сортировать
     * @param order сортировка по возрастанию если true, пример ( 1 3 4 5 8 32 98 )
     * @return Cursor
     */
    public Cursor getAllRows(String tableName, String where, String[] whereArgs, String[] columnNames, boolean[] order) {
        String orderBy = "";

        for (int i = 0; i < columnNames.length; i++) {
            String orderClause = order[i] ? "ASC":"DESC";
            orderBy = orderBy + columnNames[i] + " " + orderClause;
            if(i != columnNames.length-1){
                orderBy = orderBy.concat(", ");
            }
        }
        return mDBHelper.getReadableDatabase().query(true, tableName, null, where, whereArgs, null, null, orderBy, null);
    }

    /**
     * Получаем все записи из таблицы
     * @param tableName имя таблицы
     * @param column строку, которую вытаскиваем с базы
     * @param where условие, по которому будут запрошены записи
     * @param whereArgs массив аргументов
     * @param columnName имя столбца по которому сортировать
     * @param order сортировка по возрастанию если true, пример ( 1 3 4 5 8 32 98 )
     * @param distinct результаты не будут дублироваться
     * @return Cursor
     */
    public Cursor getAllRows(String tableName, String[] column,  String where, String[] whereArgs, String columnName, boolean order, boolean distinct) {
        String orderBy = null;

        if (columnName != null) {
            String orderClause = order ? "ASC":"DESC";
            orderBy = columnName + " " + orderClause;
        }
        return mDBHelper.getReadableDatabase().query(distinct, tableName, column, where, whereArgs, columnName, null, orderBy, null);
    }

    /**
     * Получаем все записи из таблицы
     * @param tableName имя таблицы
     * @param column строку, которую вытаскиваем с базы
     * @param distinct результаты не будут дублироваться
     * @return Cursor
     */
    public Cursor getAllRows(String tableName, String[] column, boolean distinct) {
        return mDBHelper.getReadableDatabase().query(distinct, tableName, column, null, null, null, null, null, null);
    }


    /**
     * Получаем все записи из таблицы
     * @param tableName имя таблицы
     * @param where условие, по которому будут запрошены записи
     * @param whereArgs массив аргументов
     * @return Cursor
     */
    public Cursor getAllRows(String tableName, String where, String[] whereArgs) {
        return mDBHelper.getReadableDatabase().query(tableName, null, where, whereArgs, null, null, null);
    }


    /**
     * Получаем все записи из таблицы
     * @param tableName имя таблицы
     * @return Cursor
     */
    public Cursor getAllRows(String tableName) {
        return mDBHelper.getReadableDatabase().query(tableName, null, null, null, null, null, null);
    }

    /**
     * Добавляем строку с заменной уже существующей
     * @param tableName имя таблицы
     * @param contentValues ContentValues
     * @return long
     */
    public long insertRow(String tableName, ContentValues contentValues) {
        return mDBHelper.getWritableDatabase().insertWithOnConflict(tableName, null, contentValues,
                SQLiteDatabase.CONFLICT_REPLACE);
    }


    /**
     * Обновляем строку
     * @param tableName имя таблицы
     * @param contentValues ContentValues
     * @param columnName имя столбца
     * @param id id записи
     * @return int
     */
    public int updateRow(String tableName, ContentValues contentValues, String columnName, int id) {
        return mDBHelper.getWritableDatabase().update(tableName, contentValues, columnName + " = " + id, null);
    }

    /**
     * Удаляем строку
     * @param tableName имя таблицы
     * @param where условие, по которому будут запрошены записи
     * @param whereArgs массив аргументов
     * @return число удаленных строк
     */
    public int deleteRow(String tableName, String where, String[] whereArgs) {
        return mDBHelper.getWritableDatabase().delete(tableName, where, whereArgs);
    }

    /**
     * Очищаем таблицу от записей
     * @param tableName имя таблицы
     * @param where условие, null для удаления всех строк
     * @param whereArgs агрументы условия
     * @return int число удаленных строк
     */
    public int clearTable(String tableName, String where, String[] whereArgs) {
        return mDBHelper.getWritableDatabase().delete(tableName, where, whereArgs);
    }

    /**
     * Очищаем таблицу от записей
     * @param tableName имя таблицы
     * @return int число удаленных строк
     */
    public int clearTable(String tableName) {
        return mDBHelper.getWritableDatabase().delete(tableName, null, null);
    }

    /**
     * Получаем все записи из таблицы
     * @param tableName имя таблицы
     * @param columnName имя столбца по которому сортировать
     * @param order сортировка по возрастанию если true, пример ( 1 3 4 5 8 32 98 )
     * @return Cursor
     */
    public Cursor getAllRows(String tableName, String columnName, boolean order) {
        String orderBy = null;

        if (columnName != null) {
            String orderClause = order ? "ASC":"DESC";
            orderBy = columnName + " " + orderClause;
        }
        return mDBHelper.getReadableDatabase().query(tableName, null, null, null, null, null, orderBy, null);
    }


    private class DBHelper extends SQLiteOpenHelper {

        private static final String DB_NAME = "LombardsDbName";
        private static final int DB_VERSION = 1;

        private final String CREATE_TABLE_LOMBARDS = "create table " + TABLE_LOMBARD_LIST + "(" +
                COLUMN_BASE_ID + " integer primary key autoincrement," +
                COLUMN_LOMBARD_ID + " text," +
                COLUMN_LOMBARD_TITLE + " text," +
                COLUMN_LOMBARD_ADDRESS + " text," +
                COLUMN_LOMBARD_LICENSE  + " text," +
                COLUMN_LOMBARD_WORK_TIME + " text," +
                COLUMN_LOMBARD_DESK  + " text," +
                COLUMN_LOMBARD_IMAGES + " text," +
                COLUMN_LOMBARD_ICO + " text," +
                COLUMN_LOMBARD_REGION + " text," +
                COLUMN_LOMBARD_PHONES + " text," +
                COLUMN_LOMBARD_SITE  + " text," +
                COLUMN_LOMBARD_EMAIL + " text," +
                COLUMN_LOMBARD_PHOTOS + " text," +
                COLUMN_LOMBARD_LAT_LON + " text," +
                COLUMN_LOMBARD_DOC_LIST + " text," +
                COLUMN_LOMBARD_CREATE_DATE  + " text);";


        private final String CREATE_TABLE_FAVOURITES = "create table " + TABLE_FAVOURITES_LIST + "(" +
                COLUMN_BASE_ID + " integer primary key autoincrement," +
                COLUMN_FAVOURITES_LOMBARD_ID  + " text);";

        private final String CREATE_TABLE_LOAN_TYPES = "create table " + TABLE_LOAN_LIST + "(" +
                COLUMN_BASE_ID + " integer primary key autoincrement," +
                COLUMN_LOAN_ID + " text," +
                COLUMN_LOAN_LOMBARD_ID + " text," +
                COLUMN_LOAN_NAME  + " text);";

        private final String CREATE_TABLE_LOAN_SUB_TYPES = "create table " + TABLE_LOAN_INNER_LIST + "(" +
                COLUMN_BASE_ID + " integer primary key autoincrement," +
                COLUMN_LOAN_INNER_ID + " text," +
                COLUMN_LOAN_INNER_PARENT_ID + " text," +
                COLUMN_LOAN_INNER_NAME + " text," +
                COLUMN_LOAN_INNER_RATE  + " text);";

        private final String CREATE_TABLE_GOLD = "create table " + TABLE_GOLD + "(" +
                COLUMN_BASE_ID + " integer primary key autoincrement," +
                COLUMN_GOLD_ID + " text," +
                COLUMN_GOLD_LOMBARD_ID + " text," +
                COLUMN_GOLD_CARAT + " text," +
                COLUMN_GOLD_PRICE  + " text);";



        private DBHelper(Context context) {
            super(context, DB_NAME, null, DB_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE_LOMBARDS);
            db.execSQL(CREATE_TABLE_LOAN_TYPES);
            db.execSQL(CREATE_TABLE_LOAN_SUB_TYPES);
            db.execSQL(CREATE_TABLE_GOLD);
            db.execSQL(CREATE_TABLE_FAVOURITES);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        }
    }
}

package com.fromtheseventhsky.lombards.managers;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;

import com.fromtheseventhsky.lombards.constants.PrefConst;

import java.util.Locale;

/**
 * Created by FromTheSeventhSky in 2016.
 */
public class AppPreferenceManager implements PrefConst {

    private static AppPreferenceManager sManager;
    private final Context mContext;
    private Locale mLocale;

    private AppPreferenceManager(Context context) {
        mContext = context.getApplicationContext();
    }

    public static AppPreferenceManager getInstance(Context context) {
        if (sManager == null) {
            sManager = new AppPreferenceManager(context);
        }
        return sManager;
    }


    public void switchLocale(String lang) {
        if (lang.equalsIgnoreCase("")) return;
        mLocale = new Locale(lang);
        saveLocale(lang);
        Locale.setDefault(mLocale);
        Configuration configuration = new Configuration();
        configuration.locale = mLocale;
        mContext.getResources().updateConfiguration(configuration, mContext.getResources().getDisplayMetrics());
    }


    public void loadLocale() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        String language = prefs.getString(PREFERENCE_LANGUAGE, "");
        switchLocale(language);
    }

    public void clear() {
        SharedPreferences preferences = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        preferences.edit().clear().apply();
    }

    protected void saveLocale(String lang) {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREFERENCE_LANGUAGE, lang);
        editor.apply();
    }

    public void saveAuthCode(String code) {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(PREFERENCE_AUTH, code);
        editor.apply();
    }
    public String getAuthCode() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getString(PREFERENCE_AUTH, "");
    }

    public void saveLombardUpdateDate(long date) {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putLong(PREFERENCE_UPDATE_LOMBARD, date);
        editor.apply();
    }
    public long getLombardUpdateDate() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getLong(PREFERENCE_UPDATE_LOMBARD, 0L);
    }

    public void setNotificationEnabled(boolean state) {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFERENCE_NOTIFICATIONS_STATE, state);
        editor.apply();
    }
    public boolean isNotificationEnabled() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(PREFERENCE_NOTIFICATIONS_STATE, true);
    }

    public void setNotificationSoundEnabled(boolean state) {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean(PREFERENCE_NOTIFICATIONS_SOUND, state);
        editor.apply();
    }
    public boolean isNotificationSoundEnabled() {
        SharedPreferences prefs = mContext.getSharedPreferences(PREFERENCE_FILE_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean(PREFERENCE_NOTIFICATIONS_SOUND, true);
    }

}
